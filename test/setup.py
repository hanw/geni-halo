#
# Copyright 2014 Waltz Network Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
An example of how to remotely program routing table and address table through telnet.

*** You must use \n\r to indicate the end of command, otherwise the command
    will be buffered by telnet server on the router, but not executed.

Caveat: does not support printing of error message

"""

import telnetlib

tn = telnetlib.Telnet('127.0.0.1', '2323')

tn.write("config.parse('/tmp/s1.conf')\n\r")
tn.write("config.parse('/tmp/s2.conf')\n\r")
tn.write("config.parse('/tmp/s3.conf')\n\r")
