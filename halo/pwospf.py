## PWOSPF Messages Format
#
# All PWOSPF packets are encapsulated in a common header that is identical to
# the OSPFv2 header.
#===================================================================
#
#  0                   1                   2                   3
#  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |   Version #   |     Type      |         Packet length         |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                          Router ID                            |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                           Area ID                             |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |           Checksum            |             Autype            |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                       Authentication                          |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                       Authentication                          |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#
#===================================================================
import struct

from pox.core import core
from pox.lib.addresses import IPAddr, EthAddr, netmask_to_cidr, cidr_to_netmask
from pox.lib.packet.ethernet import ethernet, ETHER_ANY, ETHER_BROADCAST
from pox.lib.packet.packet_base import packet_base
from pox.lib.packet.ipv4 import ipv4, IP_ANY, IP_BROADCAST
from pox.lib.packet.packet_utils import checksum

log = core.getLogger()

OSPF_V2            = 2
OSPF_PROTOCOL      = 89

OSPF_AllSPFRouters = 0xe0000005 #224.0.0.5

OSPF_TYPE_HELLO    = 1
OSPF_TYPE_LSU      = 4
OSPF_NET_BROADCAST = 1
OSPF_NEIGHBOR_TIMEOUT  = 20 # seconds
OSPF_DEFAULT_HELLO_INT = 3 # seconds
OSPF_DEFAULT_LSU_INT = 2 # seconds

OSPF_TOPO_ENTRY_TIMEOUT = 35 # seconds

OSPF_DEFAULT_AUTHKEY = 0 # ignored

OSPF_MAX_HELLO_SIZE  = 1024 # bytes
OSPF_MAX_LSU_SIZE    = 1024 # bytes
OSPF_MAX_LSU_TTL     = 255

class pwospf (packet_base):
    "pwospf packet struct"

    MIN_LEN = 24

    def __init__(self, raw=None, prev=None, **kw):
        super(pwospf, self).__init__()

        self.prev = prev

        self.version = OSPF_V2
        self.type = 0
        self.len = 0
        self._rid = 0
        self.aid = 0
        self.csum = 0
        self.autype = 0
        self.audata = 0
        self.next = b''

        if raw is not None:
            self.parse(raw)

        self._init(kw)

    @property
    def rid(self):
        return self._rid

    @rid.setter
    def rid(self, rid):
        if not isinstance(rid, IPAddr):
            rid = IPAddr(rid)
        self._rid = rid

    def parse (self, raw):
        assert isinstance(raw, bytes)
        self.next = None # In case of unfinished parsing
        self.raw = raw
        dlen = len(raw)
        if dlen < self.MIN_LEN:
            self.msg('(pwospf parse) warning pwospf packet too short to parse header: data len %u' % dlen)
            return

        (self.version, self.type, self.len, self.rid, self.aid, \
            self.csum, self.autype, self.audata) = \
            struct.unpack('!BBHLLHHQ', raw[:self.MIN_LEN])

        if self.version != OSPF_V2:
            self.msg('(pwospf parse) version unknown %u' % self.version)
            return

        # checksum
        if not self.checksum(raw[self.MIN_LEN:]):
            self.msg('(pwospf parse) checksum failed')
            return

        if self.autype != 0:
            return

        #TODO: check area_id, currently assume 0
        # if route id is equal to ours, drop packet
        if self.rid == 0: #FIXME
            return

        # parse HELLO and LSU
        if (self.type == OSPF_TYPE_HELLO):
            self.next = ls_hello(raw=raw[self.MIN_LEN:], prev=self)
        elif (self.type == OSPF_TYPE_LSU):
            self.next = ls_upd(raw=raw[self.MIN_LEN:], prev=self)
        else:
            self.msg('(pwospf parse) error unknown type %u' % self.type)

        self.parsed = True

    def hdr(self, payload):
        #log.debug('%s %s %s %s %s %s' % (self.version, self.type, self.len,
        #                                self.rid, self.aid, self.autype))
        data = struct.pack('!BBHLLHHQ', self.version,
                                        self.type,
                                        self.len,
                                        self.rid.toUnsigned(networkOrder=False),
                                        self.aid,
                                        0,
                                        self.autype,
                                        0)

        self.csum = checksum(data + payload)
        s = struct.pack('!BBHLLHHQ', self.version,
                                     self.type,
                                     self.len,
                                     self.rid.toUnsigned(networkOrder=False),
                                     self.aid,
                                     self.csum,
                                     self.autype,
                                     self.audata)
        return s

    def checksum(self, payload):
        """
        calculate checksum
        """
        data = struct.pack('!BBHLLHHQ', self.version,
                                        self.type,
                                        self.len,
                                        self.rid.toUnsigned(networkOrder=False),
                                        self.aid,
                                        0,
                                        self.autype,
                                        0)
        r = checksum(data + payload)
        return r == self.csum

#
# Hello packets are PWOSPF packet type 1. These packets are sent periodically
# on all interfaces in order to establish and maintain neighbor relationships.
# In addition, Hellos broadcast enabling dynamic discovery of neighboring
# routers.
#
# HELLO Packet format
#===================================================================
#
#  0                   1                   2                   3
#  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                        Network Mask                           |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |         HelloInt              |           padding             |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#
#===================================================================
class ls_hello (packet_base):

    MIN_LEN = 8

    def __init__(self, raw=None, prev=None, **kw):
        super(ls_hello, self).__init__()

        self.prev = prev

        self._netmask = 0
        self.helloint = 0
        self.padding = 0
        self.next = b''

        if raw is not None:
            self.parse(raw)

        self._init(kw)

    @property
    def helloint(self):
        return self._helloint

    @helloint.setter
    def helloint(self, helloint):
        if not isinstance(helloint, int):
            helloint = int(helloint)
        self._helloint = helloint

    @property
    def netmask(self):
        return self._netmask

    @netmask.setter
    def netmask(self, netmask):
        if not isinstance(netmask, IPAddr):
            netmask = IPAddr(netmask)
        self._netmask = netmask

    def __str__(self):
        return "[OSPF HELLO] %s %u" % (IPAddr(self.netmask), self.helloint)

    def parse(self, raw):
        assert isinstance(raw, bytes)
        self.raw = raw
        dlen = len(raw)
        if dlen < self.MIN_LEN:
            self.msg('(hello parse) warning hello payload too short to parse header: data len %u' % (dlen))
            return

        (self.netmask, self.helloint, self.padding) = struct.unpack('!LHH', raw[:self.MIN_LEN])

        # if HELLO interval not equal to router setting, drop packet
        #FIXME

        self.parsed = True

    def hdr(self, payload):
        #log.debug('%s %s %s' % (self.netmask, self.helloint, self.padding))
        s = struct.pack('!LHH', self.netmask.toUnsigned(networkOrder=False),
                                self.helloint,
                                self.padding)
        return s

#
# LSU packets implement the flooding of link states and  are used to build and
# maintain the network topology database at each router.  Each link state
# update packet carries a collection of link state advertisements on hop
# further from its origin.  Several link state advertisements may be included
# in a single packet.
#
#===================================================================
#
#  0                   1                   2                   3
#  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |     Sequence                |          TTL                    |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                      # advertisements                         |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                                                               |
#  +-                                                            +-+
#  |                  Link state advertisements                    |
#  +-                                                            +-+
#  |                              ...                              |
#
#===================================================================
class ls_upd (packet_base):
    "pwospf ls_upd packet struct"
    MIN_LEN=8

    def __init__(self, raw=None, prev=None, **kw):
        super(ls_upd, self).__init__()

        self.prev = prev

        self.seq = 0
        self.ttl = 0
        self.n_adv = 0
        self.entries = []
        self.next = None

        if raw is not None:
            self.parse(raw)

        self._init(kw)

    def __str__(self):
        return "[OSPF LinkState Update] %u %u %u" % self.seq, self.ttl, self.n_adv

    def parse(self, raw):
        assert isinstance(raw, bytes)
        self.raw = raw
        dlen = len(raw)
        if dlen < self.MIN_LEN:
            self.msg('(ls_upd parse) warning ls_upd payload too short to parse header: data len %u' % dlen)
            return

        (self.seq, self.ttl, self.n_adv) = struct.unpack('!hHL',raw[:self.MIN_LEN])

        self.parsed = True

        if self.n_adv <= 0:
            return

        log.debug('no. of adv is %d' % self.n_adv)
        for i in range(self.n_adv):
            self.entries.append(lsa(raw=raw[ls_upd.MIN_LEN + i * lsa.MIN_LEN:], prev=self))

    def hdr(self, payload):
        s = struct.pack('!hHL', self.seq, self.ttl, self.n_adv)
        for e in self.entries:
            log.debug('%s' % e)
            s += e.pack()
        return s

#
# Each link state update packet should contain 1 or more link state
# advertisements.  The advertisements are the reachable routes directly
# connected to the advertising router.  Routes are in the form of the subnet,
# netmask and router neighor for the attached link.
#
#===================================================================
#
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                           Subnet                              |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                           Mask                                |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#  |                         Router ID                             |
#  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#
#===================================================================
class lsa (packet_base):

    MIN_LEN = 16

    def __init__(self, raw=None, prev=None, **kw):
        super(lsa, self).__init__()
        self.prev = prev

        self._subnet = None
        self._netmask = None # An IPAddr, but netmask property allows you assign
                          # dotquad string or an integer number of bits.
        self._rid = None
	self._tx_rate = 0
        self.next = None

        if raw is not None:
            self.parse(raw)

        self._init(kw)

    @property
    def subnet(self):
        return self._subnet

    @subnet.setter
    def subnet(self, subnet):
        if not isinstance(subnet, IPAddr):
            subnet = IPAddr(subnet)
        self._subnet = subnet

    @property
    def netmask(self):
        return self._netmask

    @netmask.setter
    def netmask(self, netmask):
        if not isinstance(netmask, IPAddr):
            netmask = IPAddr(netmask)
        self._netmask = netmask

    @property
    def rid(self):
        return self._rid

    @rid.setter
    def rid(self, rid):
        if not isinstance(rid, IPAddr):
            rid = IPAddr(rid)
        self._rid = rid

    @property
    def tx_rate(self):
        return self._tx_rate

    @tx_rate.setter
    def tx_rate(self, tx_rate):
        self._tx_rate = tx_rate

    def __str__(self):
        return "[OSPF LSA] subnet/netmask %s/%s, router_id %s, tx_rate 0x%010x" % (IPAddr(self.subnet).toStr(),
            netmask_to_cidr(IPAddr(self.netmask)), IPAddr(self.rid).toStr(), self.tx_rate)

    def parse(self, raw):
        assert isinstance(raw, bytes)
        self.raw = raw
        dlen = len(raw)
        if dlen < self.MIN_LEN:
            self.msg('(lsa parse) warning lsa payload too short to parse header: data len %u' % dlen)
            return

        (self.subnet, self.netmask, self.rid, self.tx_rate) = struct.unpack('!LLLL', raw[:self.MIN_LEN])
        self.parsed = True

    def hdr(self, payload):
        s = struct.pack('!LLLL', self.subnet.toUnsigned(networkOrder=False),
                                self.netmask.toUnsigned(networkOrder=False),
                                self.rid.toUnsigned(networkOrder=False),
                                self.tx_rate)
        return s


