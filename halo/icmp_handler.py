#
# Copyright 2014 Waltz Network Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This module handles icmp packets for routers. It works as follows:
- if the icmp packet is for one of the routers interface, send an icmp reply,
  install a rule for future icmp packets.
- if the icmp packet is for a destination in the routing table, forward the icmp
  packet to the corresponding port, set a rule for future icmp packets.
- if the icmp packet is for unknown destination, send icmp unreachable message.
"""

from pox.core import core
log = core.getLogger()

import pox.lib.packet as pkt
import pox.openflow.libopenflow_01 as of
#from halo.arp_handler import send_arp_request
from pox.lib.util import dpid_to_str
from generic_handler import GenericHandler
from halo_utils import ipv4_apply_mask
from pox.lib.addresses import netmask_to_cidr


class ICMPHandler(GenericHandler):
    def __init__(self):
        self.dpid = None
        core.addListeners(self)

    def _handle_GoingUpEvent (self, event):
        core.openflow.addListeners(self)
        self.debug("ICMP Up...")

    def _handle_PacketIn(self, event):
        ev = self.parse_event(event)
        dpid = ev['dpid']
        packet = ev['packet']

        self.dpid = dpid
        interfaces = core.router[dpid].interfaces
        rtable = core.router[dpid].routing_table
        atable = core.router[dpid].arp_table

        ipp_echo = packet.find('ipv4')
        if not ipp_echo: return

        icmp = packet.find('icmp')
        if not icmp: return

        #self.print_packet('icmp', ipp_echo)

        if icmp.type == pkt.ICMP.TYPE_ECHO_REQUEST:
            intf = interfaces.find_by_ip(ipp_echo.dstip)
            if intf:
                self.handle_icmp_request(ev)
            else:
                self.handle_missed_icmp_request(ev, rtable, atable, interfaces, ipp_echo)
        elif icmp.type == pkt.ICMP.TYPE_ECHO_REPLY:
            intf = interfaces.find_by_ip(ipp_echo.dstip)
            if intf:
                pass
            else:
                self.handle_missed_icmp_request(ev, rtable, atable, interfaces, ipp_echo)

    def decrease_ttl(self, ipp):
        ipp.ttl = ipp.ttl - 1
        ipp.csum = ipp.checksum()
        return ipp

    def forward_icmp(self, ev, port, hwdst, ipp_echo):
        ethp = ev['packet']
        icmp = ipp_echo.find('icmp')
        new_ipp_echo = self.decrease_ttl(ipp_echo)
        new_ipp_echo.payload = icmp
        ethp.dst = hwdst
        ethp.payload = new_ipp_echo
        actions = of.ofp_action_output(port=port)
        self.packet_out(ev, of.OFPP_NONE, ethp, actions)

    def handle_missed_icmp_request(self, ev, rtable, atable, interfaces, ipp_echo):
        port = ev['inport']
        dpid = ev['dpid']
        router = core.router[dpid]
        route = rtable.find_by_ip(ipp_echo.dstip)
        if route is not None:
            log.info(router.topology.nodeList[router.router_id].shortestPaths.keys())
            log.info(router.lsdb.get_router_id_by_ip(ipp_echo.dstip))
            dst_router_id = router.lsdb.get_router_id_by_ip(ipp_echo.dstip)
            if dst_router_id:
                next_hop_router_id = router.topology.nodeList[router.router_id].shortestPaths[dst_router_id][1]
                next_hop_router_ip = router.interfaces.find_neighbor_by_router_id(next_hop_router_id)
            else:
                next_hop_router_ip = ipp_echo.dstip
            mac = atable.lookup(next_hop_router_ip)
            if mac is None:
                for port in interfaces.iterkeys():
                    if port[0] == 65534:
                        continue
                    intf = interfaces.find_by_port(port[0])
                    if ipv4_apply_mask(intf.ip, netmask_to_cidr(intf.netmask)) == ipv4_apply_mask(ipp_echo.dstip, netmask_to_cidr(intf.netmask)):
                        arp = self.create_arp_request(intf.ip, ipp_echo.dstip, intf.hwaddr)
                        actions = of.ofp_action_output(port=port[0])
                        self.packet_out(ev, of.OFPP_NONE, arp, actions)
            else:
                self.forward_icmp(ev, mac.port, mac.hwaddr, ipp_echo)
        else:
            # unreacheable
            pass #drop

    def handle_icmp_request(self, ev):
        packet = ev['packet']
        port = ev['inport']
        ipp_echo = packet.find('ipv4')
        icmp_echo = packet.find('icmp')
        dstip = ipp_echo.srcip
        srcip = ipp_echo.dstip
        payload = icmp_echo.payload
        eth = self.create_icmp_reply(packet, srcip=srcip, dstip=dstip, payload=payload)
        actions = of.ofp_action_output(port=of.OFPP_IN_PORT)
        self.packet_out(ev, port, eth, actions)

def launch():
  core.registerNew(ICMPHandler)
