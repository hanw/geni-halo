#
# CopyRight(c) 2014 Waltz Networks Inc.
#
from constants import UINT32_MAX, DEFAULT_ROUTE, COOKIE_DEFAULT_ID
import datetime
from pox.lib.addresses import IPAddr
from halo_utils import ipv4_apply_mask, nw_addr_aton

class RouteEntry(object):
    def __init__(self, route_id, subnet, netmask, split_ratios, mac=None, port=None):
        super(RouteEntry, self).__init__()
        self.route_id = route_id
        self.subnet = subnet
        self.netmask = netmask
        self.split_ratios = split_ratios
        self.curr_split_ratios = {}
        self.flow_cnt = {}
        self.total_flow = 0
        self.rate = 0

    def __repr__(self):
        str = '%4d %16s/%2s %s %s' % (self.route_id,
                                         self.subnet,
                                         self.netmask,
                                         self.split_ratios,
                                         self.curr_split_ratios)
        return str

    # greedily select next hop based on flow count
    # NOTE: this approach does not take account of flow size
    def select_next_hop(self, flow, inport, router):
        # iterate all split ratios
        print 'select_next_hop is triggered'
        next_hop = None
        for i in self.split_ratios:
            src_id = router.lsdb.get_router_id_by_ip(flow.srcip)
            ip = router.interfaces.find_neighbor_by_router_id(src_id)
            if (ip is not None) and (ipv4_apply_mask(i, self.netmask) == ipv4_apply_mask(ip, self.netmask)):
                continue
            else: 
                arp_entry = router.arp_table[i] 
                if arp_entry.port != inport:
                    next_hop = i
                if i not in self.curr_split_ratios.keys():
                    self.curr_split_ratios[i] = 0
                    self.flow_cnt[i] = 0
                # if current next_hop split ratio is less than target
                if ((self.curr_split_ratios[i] < self.split_ratios[i]) or (self.split_ratios[i] == 1)) and (arp_entry.port != inport):
                    if i not in self.flow_cnt:
                        self.flow_cnt[i] = 0
                        #self.flow_cnt[i] += 1
                        #self.total_flow += 1
                    break # select current i as next_hop
        return next_hop

    #FIXME: split ratio is computed based on the flow counts, not flow rate.
    #       assuming each flow is of same rate.
    def refresh_split_ratio(self):
        if self.total_flow == 0:
            return
        for i in self.curr_split_ratios:
            self.curr_split_ratios[i] = self.flow_cnt[i] / float(self.total_flow)

    def compute_curr_split_ratios(self, router):
        flow_cnt = {}
        total_flow = 0
        for k, v in router.policy.flow_table.iteritems():
            if ipv4_apply_mask(k.dstip, self.netmask) == self.subnet:
                total_flow += v[3]
                for i in self.split_ratios:
                    if i not in flow_cnt:
                        flow_cnt[i] = 0
                    if i not in self.curr_split_ratios:
                        self.curr_split_ratios[i] = 0
                        self.flow_cnt[i] = 0
                    arp_entry = router.arp_table[i]
                    if v[0] == arp_entry.port:
                        flow_cnt[i] += v[3]
        if total_flow == 0:
            self.total_flow = 1
            return
        else:
            for i in self.curr_split_ratios:
                self.flow_cnt[i] = flow_cnt[i]
                self.total_flow = total_flow
                self.curr_split_ratios[i] = float(flow_cnt[i] / total_flow)

    # redistribute flow based on new split ratio
    def redist_flows(self, router):
        if self.total_flow == 0:
            return
        for k, v in router.policy.flow_table.iteritems():
            if ipv4_apply_mask(k.dstip, self.netmask) == self.subnet:
                for i in self.split_ratios:
                    src_id = router.lsdb.get_router_id_by_ip(k.srcip)
                    ip = router.interfaces.find_neighbor_by_router_id(src_id)
                    if (ip is not None) and (ipv4_apply_mask(i, self.netmask) == ipv4_apply_mask(ip, self.netmask)):
                        continue
                    else: 
                        prev_port = 0
                        arp_entry = router.arp_table[i]
                        if i not in self.curr_split_ratios.keys():
                            self.curr_split_ratios[i] = 0
                            self.flow_cnt[i] = 0
                        if (self.curr_split_ratios[i] < self.split_ratios[i]) and (k.inport != arp_entry.port):
                            if v[0] != arp_entry.port:
                            #router.policy.flow_table[k] = (arp_entry.port, arp_entry.hwaddr)
                                router.policy.flow_table[k][0] = arp_entry.port
                                router.policy.flow_table[k][1] = arp_entry.hwaddr
                                self.compute_curr_split_ratios(router)
        #router.policy.update_policy()
        self.compute_curr_split_ratios(router)

class RoutingTable(dict):
    def __init__(self):
        super(RoutingTable, self).__init__()
        self.route_id = 1

    def __repr__(self):
        o = []
        for k, e in self.iteritems():
            o.append((k, e.__repr__()))

        o.sort()
        o = [e[1] for e in o]
        o.insert(0, "---- Routing Table ----")
        if len(o) == 1:
            o.append("<< Empty >>")
        else:
            o.insert(1, "%4s %19s %19s" %
                    ('id', 'route', 'split ratios'))
        return "\n".join(o)

    def add(self, dest, split_ratios, router):
        if dest == DEFAULT_ROUTE:
            subnet = 0
            netmask = 0
        else:
            subnet, netmask, _ = nw_addr_aton(dest)

        ip_str = IPAddr(subnet).toStr()
        key = '%s/%d' % (ip_str, netmask)

        total_flow = 0
        curr_split_ratios = {}
        flow_cnt = {}
        rate = 0
        if key not in self:
            route_id = self.route_id
        else:
            route_id = self[key].route_id
            total_flow = self[key].total_flow
            curr_split_ratios = self[key].curr_split_ratios
            flow_cnt = self[key].flow_cnt
        if key in router.policy.flow_table.routing_table:
            rate = router.policy.flow_table.routing_table[key].rate

        if key not in self:
            self.route_id += 1

        routing_data = RouteEntry(route_id, subnet, netmask, split_ratios)
        self[key] = routing_data
        self[key].total_flow = total_flow
        self[key].curr_split_ratios = curr_split_ratios
        self[key].flow_cnt = flow_cnt
        self[key].rate = rate
	if router.policy.flow_table is not None:
            #print ('###############')
            #print self[key].total_flow
            #print self[key].flow_cnt
            #print self[key].curr_split_ratios
            #print self[key].split_ratios
            self[key].redist_flows(router)
            #self[key].refresh_split_ratio()

        if self.route_id == COOKIE_DEFAULT_ID:
            self.route_id = 1
        print '*********************'
        print datetime.datetime.time(datetime.datetime.now())
        print routing_data
        return routing_data

    def delete(self, route_id):
        for key, value in self.items():
            if value.route_id == route_id:
                del self[key]
                return

    def get_split_ratios(self, ip, netmask):
        for key, value in self.items():
            if value.subnet == ip and value.netmask == netmask:
                return value.split_ratios
        return None

    def get_next_hop(self):
        return [data.next_hop for data in self.values()]

    def find_by_ip(self, ip):
        for key, value in self.items():
            if ipv4_apply_mask(ip, value.netmask) == value.subnet:
                return value
        return None

    def find_by_cidr(self, subnet, mask):
        for key, value in self.items():
            if value.subnet == subnet and value.netmask == mask:
                return value
        return None

    def find_by_port(self, port):
        routes = []
        for key, value in self.items():
            if value.next_hop_port == port:
                routes.append(routes)
        return routes

    def get_rate(self, subnet, mask):
        route = self.find_by_cidr(subnet, mask)
        if route:
            return route.rate
        else:
            return 0
