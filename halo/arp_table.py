#
# CopyRight(c) 2014 Waltz Networks Inc.
#
import time
from pox.core import core
from pox.lib.addresses import EthAddr, IPAddr

log = core.getLogger()


class ARPEntry(object):
    def __init__(self, port, hwaddr, age_max, static=None, flood=None):
        super(ARPEntry, self).__init__()
        self.port = port
        self.hwaddr = EthAddr(hwaddr)
        self.age_max = age_max
        self.timeout = time.time() + self.age_max
        self.static = False
        self.flood = True

        if static is not None:
            self.static = static
        if flood is not None:
            self.flood = flood

    def update(self, port, hwaddr):
        self.port = port
        self.hwaddr = EthAddr(hwaddr)
        self.timeout = time.time() + self.age_max

    @property
    def is_expired(self):
        if self.static:
            return False
        return time.time() > self.timeout


class ARPTable(dict):
    # constants
    def __init__(self):
        self.DEFAULT_AGE_MAX = 300

    # pretty print
    def __repr__(self):
        o = []
        for k, e in self.iteritems():
            t = int(e.timeout - time.time())
            if t < 0:
                t = "X"
            else:
                t = str(t) + "s left"
            if e.static:
                t = "-"
            hwaddr = e.hwaddr
            port = e.port
            if hwaddr is True:
                hwaddr = "<Switch hwaddr>"
            o.append((k, "%-17s %-20s %3s %3s" % (k, hwaddr, t, port)))
        o.sort()
        o = [e[1] for e in o]
        o.insert(0, "-- ARP Table -----")
        if len(o) == 1:
            o.append("<< Empty >>")
        return "\n".join(o)

    # update an entry or create it.
    def update(self, port, ipaddr, hwaddr):
        if self.has_key(ipaddr):
            entry = self[ipaddr]
            entry.update(port, hwaddr)
        else:
            new_entry = ARPEntry(port, hwaddr, self.DEFAULT_AGE_MAX)
            self[ipaddr] = new_entry

    # search for an entry
    def lookup(self, ipaddr):
        if self.has_key(ipaddr):
            return self[ipaddr]
        return None

    # delete an entry statically
    def delete(self, key):
        key = IPAddr(key)
        dict.__delitem__(self, key)

    # age out an entry
    def age(self):
        for k, e in self.items():
            if e.is_expired:
                self.delete(k)
