#
# CopyRight(c) 2014 Waltz Network Inc.
#
import threading

import asyncore
import pox.openflow.libopenflow_01 as of
from arp_table import ARPTable
from routing_table import RoutingTable
from interface_table import Interfaces, Interface
from pwospf_lsdb import LSDB
from pwospf_utils import Pwospfd
from adjacency_list import Topology
from priodict import priorityDictionary
from pox.core import core
from pox.lib.addresses import EthAddr, IPAddr
from pox.lib.revent import EventMixin

from pyretic.core.runtime import Runtime
from pyretic.backend.backend import Backend, BACKEND_PORT

from halo.pyretic_policy import PyreticPolicy
from halo.pyretic_handler import PyreticHandler

log = core.getLogger()

# assume we never connect a controller to more than 100 datapaths
def get_backend_port(dpid):
    port = BACKEND_PORT + dpid % 100
    log.info('connected with router %s at %s' %(dpid, port))
    return port

class Routed(EventMixin):
    def __init__(self):
        self.routers = {}
        self.pyretic_handlers = {}
        core.openflow.addListeners(self)
        core.register('router', self.routers)

    def _handle_ConnectionUp(self, event):
        assert event.dpid not in self.routers

        log.debug("Connecting %s" % event.connection)
        #self.create_router(event)
        self.routers[event.dpid] = Router(event.dpid, event.ofp.ports, event.connection)
        core.Interactive.variables['router'] = self.routers
        self.create_pyretic_handler(event.dpid)

        # send packets to routers
#        msg = of.ofp_flow_mod(match = of.ofp_match())
#        msg.actions.append(of.ofp_action_output(port = of.OFPP_CONTROLLER))
#        self.routers[event.dpid].connection.send(msg)

        # add network info to pyretic runtime
        this_handlers = self.pyretic_handlers[event.dpid]
        this_handlers.send_to_pyretic(['switch','join',event.dpid,'BEGIN'])
        for port in event.ofp.ports:
            if port.port_no <= of.OFPP_MAX:
                CONF_UP = not 'OFPPC_PORT_DOWN' in self.active_ofp_port_config(port.config)
                STAT_UP = not 'OFPPS_LINK_DOWN' in self.active_ofp_port_state(port.state)
                this_handlers.send_to_pyretic(['port','join',event.dpid, port.port_no, CONF_UP, STAT_UP])
        this_handlers.send_to_pyretic(['switch','join',event.dpid,'END'])

    def _handle_ConnectionDown(self, event):
        assert event.dpid in self.routers
        del self.routers[event.dpid]
        del self.pyretic_handlers[event.dpid]

    def create_pyretic_handler(self, dpid):
        #class asyncore_loop(threading.Thread):
            #def run(self):
                #asyncore.loop()

        psock = get_backend_port(dpid)
        self.pyretic_handlers[dpid] = PyreticHandler(port=psock,
                                                     router=self.routers[dpid])

        #al = asyncore_loop()
        #al.daemon=True
        #al.start()

    def active_ofp_port_config(self,configs):
        active = []
        for (config,bit) in of.ofp_port_config_rev_map.items():
            if configs & bit:
                active.append(config)
        return active

    def active_ofp_port_state(self,states):
        """get active ofp port state values
        NOTE: POX's doesn't match ofp_port_state_rev_map"""
        active = []
        for (state,bit) in of.ofp_port_state_rev_map.items():
            if states & bit:
                active.append(state)
        return active

# Router on top of Openflow
class Router(EventMixin):
    def __init__(self, dpid, ports, connection):
        super(Router, self).__init__()

        # Router internal states
        self.dpid = dpid
        self.connection = connection
        self.router_id = None
        self.interfaces = Interfaces()
        self.arp_table = ARPTable()
        self.routing_table = RoutingTable()

        self.lsdb = LSDB(dpid) # key is router_id, value is router instance
        self.pwospfd = Pwospfd(dpid)

        # used by dijkstra
        self.topology = Topology() # local view of global topology

        # populate interface table
        #for idx, port in ports.items():
        for port in ports:
            if port.port_no <= of.OFPP_MAX: # remove the 65534 port
                self.add_port(port.hw_addr, port.port_no)
        # create runtime for pyretic based multipath and stats collection
        psock = get_backend_port(dpid)
        backend = Backend(psock)

        def run_policy():
            return self.policy

        self.policy = PyreticPolicy(self.routing_table)

        kwargs = {}
        mode = 'proactive2'
        verbose = 'high'    #FIXME
        self.runtime = Runtime(backend, run_policy, kwargs, mode, verbose)

    def __repr__(self):
        o = []
        o.append(self.arp_table.__repr__()+'\n')
        o.append(self.routing_table.__repr__()+'\n')
        o.append(self.interfaces.__repr__()+'\n')
        o.append(self.policy.flow_table.__repr__()+'\n')
        return "\n".join(o)

    def add_port(self, hwaddr, port_no):
        intf = Interface(hwaddr, port_no)
        self.interfaces.add(intf)

    def add_arp_entry(self):
        pass

    #def add_route(self, route, next_hop):
        #self.routing_table.add(route, next_hop)
        #self.stats.set_stats(route)

    def set_ip(self, port, ip, netmask):
        intf = self.interfaces.find_by_port(port)
        if intf:
            #TODO: check ip and netmask validity
            intf.ip = ip
            intf.netmask = netmask
        else:
            log.info('unable to find port %s' % port)
            pass

        # use the maximum known port ip as router id
        # to be consistent with scone, but ospf standard uses eth0
        # We can change later
        if self.router_id is None:
            self.router_id = IPAddr(ip)
            self.topology.addNode(self.router_id)
        else:
            self.topology.nodeList.pop(self.router_id, None)
            tmp = self.interfaces.find_by_ip(self.router_id)
            if port > tmp[0].port:
                self.router_id = IPAddr(ip)
                self.topology.addNode(self.router_id)

    def get_ospf_helloint(self):
        return self.pwospfd.helloint

    def set_ospf_helloint(self, value):
        self.pwospfd.change_helloint(value)

    def set_ospf_lsuint(self, value):
        self.pwospfd.change_lsuint(value)

def launch(settings=None):
    class async_loop(threading.Thread):
        def run(self):
            asyncore.loop()

    Routed() # run routed thread

    al = async_loop()
    #al.daemon = True
    al.start()

