#
#
#
from pox.core import core
from pox.lib.addresses import IPAddr, EthAddr
from pox.lib.packet.arp import arp
from pox.lib.packet.ethernet import ethernet
from pox.lib.util import dpid_to_str

import time
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt
# Maximum number of packet to buffer on a switch for an unknown IP
MAX_BUFFERED_PER_IP = 5
# Maximum time to hang on to a buffer for an unknown IP in seconds
MAX_BUFFER_TIME = 5


log = core.getLogger()


class GenericHandler(object):
    def __init__(self):
        self.dpid = None

    def __str__(self):
        if self.dpid is None:
            d = str(self.dpid)
        else:
            d = dpid_to_str(self.dpid)
        return "[%s]" % d

    def debug(self, m):
        log.debug(str(self) + " " + str(m))

    def info(self, m):
        log.info(str(self) + " " + str(m))

    def err(self, m):
        log.info(str(self) + " " + str(m))

    def print_packet(self, packet_type, packet):
        if packet_type == 'icmp':
            icmp = packet.find('icmp')
            self.print_icmp(packet.srcip, packet.dstip, icmp)
        if packet_type == 'arp':
            pass
        if packet_type == 'hello':
            self.print_hello(packet.srcip, packet.dstip)

    def print_icmp(self, src_ip, dst_ip, icmp):
        if icmp.type == pkt.ICMP.TYPE_ECHO_REQUEST:
            log.debug("ICMP ECHO from [%s] to [%s], which %s" %
                        (src_ip, dst_ip, icmp.__str__()))
        if icmp.type == pkt.ICMP.TYPE_ECHO_REPLY:
            log.debug('ICMP REPLY from [%s] to [%s], with %s' %
                        (src_ip, dst_ip, icmp.__str__()))

    def print_hello(self, src_ip, dst_ip):
        log.info('HELLO from [%s] -> [%s]' % (src_ip, dst_ip))

    def parse_event(self, event):
        o = {}
        o['connection']  = event.connection
        o['dpid'] = event.connection.dpid
        o['inport'] = event.port
        o['packet'] = event.parsed
        o['ofp'] = event.ofp
        return o

    # create arp reply packet
    def create_arp_reply(self, packet, hwaddr):
        r = arp()
        r.opcode = arp.REPLY
        r.hwdst = packet.hwsrc
        r.protodst = packet.protosrc
        r.hwsrc = EthAddr(hwaddr)
        r.protosrc = packet.protodst
        e = ethernet(type=ethernet.ARP_TYPE, src=r.hwsrc, dst=r.hwdst)
        e.payload = r
        return e

    # create arp request packet
    def create_arp_request(self, srcip, dstip, hwaddr):
        from pox.lib.packet.ethernet import ETHER_BROADCAST
        r = arp()
        r.opcode = arp.REQUEST
        r.hwdst = ETHER_BROADCAST
        r.protodst = IPAddr(dstip)
        r.hwsrc = EthAddr(hwaddr)
        r.protosrc = IPAddr(srcip)
        e = ethernet(type=ethernet.ARP_TYPE, src=r.hwsrc, dst=r.hwdst)
        e.payload = r
        return e

    def create_icmp_reply(self, packet, srcip=None, dstip=None, payload=None):
        # make reply
        icmp_reply = pkt.icmp()
        icmp_reply.type = pkt.TYPE_ECHO_REPLY
        icmp_reply.payload = payload

        # make ip
        ipp_reply = pkt.ipv4()
        ipp_reply.protocol = ipp_reply.ICMP_PROTOCOL
        ipp_reply.srcip = srcip
        ipp_reply.dstip = dstip

        # make ether
        e = pkt.ethernet()
        e.src = packet.dst
        e.dst = packet.src
        e.type = e.IP_TYPE

        # hook them up...
        ipp_reply.payload = icmp_reply
        e.payload = ipp_reply
        return e

    def packet_out(self, ev, out_port, eth_frame, actions):
        connection = ev['connection']
        msg = of.ofp_packet_out()
        msg.data = eth_frame.pack()
        msg.actions.append(actions)
        msg.in_port = out_port
        connection.send(msg)

#    def flow_mod(self, connection, packet, matches, actions):
#        msg = of.ofp_flow_mod()
#        msg.match = matches
#        msg.actions = actions
#        msg.data = packet.pack()
#        connection.send(msg)
#
    def send_lost_buffers (self, dpid, ipaddr, macaddr, port):
        """
        We may have "lost" buffers -- packets we got but didn't know
        where to send at the time.  We may know now.  Try and see.
        """
        if self.lost_buffers == None:
            return

        if (dpid,ipaddr) in self.lost_buffers:
            # Yup!
            bucket = self.lost_buffers[(dpid,ipaddr)]
            del self.lost_buffers[(dpid,ipaddr)]
            log.debug("Sending %i buffered packets to %s from %s"
                % (len(bucket),ipaddr,dpid_to_str(dpid)))
            for _,buffer_id,in_port in bucket:
                po = of.ofp_packet_out(buffer_id=buffer_id,in_port=in_port)
                po.actions.append(of.ofp_action_dl_addr.set_dst(macaddr))
                po.actions.append(of.ofp_action_output(port = port))
                core.openflow.sendToDPID(dpid, po)

    def add_to_lost_buffers(self, ev, dstaddr):
        dpid = ev['dpid']
        ofp = ev['ofp']
        inport = ev['inport']

        if self.lost_buffers == None:
            return

        if (dpid, dstaddr) not in self.lost_buffers:
            self.lost_buffers[(dpid, dstaddr)] = []
        bucket = self.lost_buffers[(dpid,dstaddr)]
        entry = (time.time() + MAX_BUFFER_TIME, ofp.buffer_id, inport)
        bucket.append(entry)
        while len(bucket) > MAX_BUFFERED_PER_IP: del bucket[0]

