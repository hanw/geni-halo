#
# CopyRight(c) 2014 Waltz Network Inc.
#

from pox.core import core
from pox.lib.addresses import EthAddr, IPAddr
from pwospf_utils import NeighborRouter
from multi_key_dict import multi_key_dict
from datetime import datetime

log = core.getLogger()


class Interface(object):
    def __init__(self, hwaddr, port, ip=None, netmask=None):
        if isinstance(hwaddr, EthAddr):
            self.hwaddr = hwaddr
        else:
            msg = 'TypeError: %s' % hwaddr
            raise Exception(msg=msg)

        self._ip = ip
        self._netmask = netmask
        self._port = port
        self.neighbors = []

    @property
    def ip(self):
        return self._ip

    @ip.setter
    def ip(self, ip):
        if not isinstance(ip, IPAddr):
            ip = IPAddr(ip)
        self._ip = ip

    @property
    def netmask(self):
        return self._netmask

    @netmask.setter
    def netmask(self, netmask):
        if not isinstance(netmask, IPAddr):
            netmask = IPAddr(netmask)
        self._netmask = netmask

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, port):
        self._port = port

    def has(self, hwaddr):
        return self.hwaddr == hwaddr

    def find_neighbor_by_ip(self, ip):
        for k in self.neighbors:
            if IPAddr(k.ip) == IPAddr(ip):
                return k
        return None

    def find_neighbor_by_router_id(self, router_id):
        for k in self.neighbors:
            log.info('%s %s' % (k.router_id, router_id))
            if IPAddr(k.router_id) == IPAddr(router_id):
                return k
        return None

    def add_neighbor(self, router_id, ip):
        router = NeighborRouter(router_id=router_id, ip=ip, last_rcvd_hello=datetime.now())
        self.neighbors.append(router)

    def del_neighbor(self):
        pass

# interface table supports the following operation
# match object by multiple key
# insert object
# remove object
class Interfaces(multi_key_dict):
    def __init__(self):
        super(Interfaces, self).__init__()

    def __repr__(self):
        o = []
        for e in self.itervalues():
            hwaddr = e.hwaddr
            ip = e.ip
            netmask = e.netmask
            port = e.port
            o.append((e.port, "%-17s %-20s %12s %12s" %(port, hwaddr, ip, netmask)))
        o.sort()
        o = [e[1] for e in o]
        o.insert(0, "-- Interface Table --")
        if len(o) == 1:
            o.append("<< Empty >>")
        return '\n'.join(o)

    def add(self, intf):
        if intf.ip is None:
            self[[intf.hwaddr, intf.port]] = intf
        else:
            # FIXME: update port ip address does not remove the old ip address.
            self[intf.ip] = intf

    def add_neighbor(self, intf):
        self.neighbors.append(intf)

    def delete(self, key):
        # FIXME: pop ip key
        del self[key]

    # handle to_me? and icmp_request
    def find_by_port(self, port):
        port = int(port)
        intf = None
        if self.has_key(port):
            intf = self[port]
        return intf

    # handle should_forward?
    def find_by_ip(self, ip):
        o=[]
        for e in self.values():
            if e.ip == ip:
                o.append(e)
        return o

    # find neighbor by router_id
    def find_neighbor_by_router_id(self, router_id):
        for e in self.itervalues():
            for k in e.neighbors:
                if k.rid == router_id:
                    return k.ip

    # find port by neighbor router id
    def find_port_by_neighbor_router_id(self, router_id):
        for e in self.itervalues():
            for k in e.neighbors:
                if k.rid == router_id:
                    return e.port

    # handle resolve_next_hop
    def find_by_prefix(self, ip):
        pass

    # handle arp
    def find_by_port_and_ip(self, port, ip):
        if self.has_key(port):
            if self[port].ip == ip:
                return self[port]

    def find_neighbor_by_ip(self, port, nbr_ip):
        for v in self[port].nbrs:
            if v.has_nbr(nbr_ip):
                v.update_timer()

    def get_all_intfs(self):
        o = []
        for e in self.values():
            if e.port != 65534:
                o.append(e)
        return o
