#
# Copyright(c) 2014 Waltz Networks Inc.
#

from pox.core import core
from pox.lib.addresses import IPAddr, EthAddr
from pox.lib.packet.arp import arp
from pox.lib.packet.ethernet import ethernet
from pox.lib.recoco import Timer
from pox.lib.util import dpid_to_str
import pox.openflow.libopenflow_01 as of
from generic_handler import GenericHandler
from halo_utils import ipv4_apply_mask

log = core.getLogger()


class ARPHandler(GenericHandler):
    def __init__(self):
        super(ARPHandler, self).__init__()
#        self.exp_timer = Timer(5, self.age, recurring=True)
        core.addListeners(self)

    def age(self):
        if core.router is None:
            return
        for idx, router in core.router.iteritems():
            router.arp_table.age()

    def sanity_check(self, arpp):
        # sanity checks
        passed = True
        if arpp.prototype != arp.PROTO_TYPE_IP:
            passed = False
        if arpp.hwtype != arp.HW_TYPE_ETHERNET:
            passed = False
        if arpp.protosrc == 0:
            passed = False
        return passed

    def is_garp(self, arpp):
        return arpp.protosrc == arpp.protodst

    def is_arp_request(self, arpp):
        return arpp.opcode == arp.REQUEST

    def is_arp_reply(self, arpp):
        return arpp.opcode == arp.REPLY

    def _handle_GoingUpEvent(self, event):
        core.openflow.addListeners(self)
        self.debug('ARP up...')

    def _handle_ConnectionUp(self, event):
        pass

    def _handle_PacketIn(self, event):
        ev = self.parse_event(event)

        if not ev['packet'].parsed:
            log.warning('%s: ignoring unparsed packet', dpid_to_str(ev['dpid']))
            return

        a = ev['packet'].find('arp')
        if not a:
            return

        if not self.sanity_check(a):
            return

        if self.is_garp(a):
            self.handle_garp(event, a)
        elif self.is_arp_request(a):
            self.handle_arp_request(ev, a)
        elif self.is_arp_reply(a):
            self.handle_arp_reply(event, a)
        else:
            pass

    def handle_garp(self, event, packet):
        pass

    def handle_arp_request(self, ev, packet):
        dpid = ev['dpid']
        port = ev['inport']
        interfaces = core.router[dpid].interfaces
        arp_table = core.router[dpid].arp_table
        routing_table = core.router[dpid].routing_table
        ip = packet.protodst
        intf = interfaces.find_by_port_and_ip(port, ip)
        if intf:
            arp_table.update(port, packet.protosrc, packet.hwsrc)
            route = routing_table.find_by_ip(packet.protosrc)
            for key, value in route.split_ratios.items():
                if ipv4_apply_mask(key, route.netmask) == route.subnet:
                    route.split_ratios[packet.protosrc] = route.split_ratios.pop(key)
            arp_reply = self.create_arp_reply(packet, intf.hwaddr)
            actions = of.ofp_action_output(port=port)
            self.packet_out(ev, of.OFPP_NONE, arp_reply, actions)
        return

    def handle_arp_reply(self, event, packet):
        arp_table = core.router[event.dpid].arp_table
        routing_table = core.router[event.dpid].routing_table
        inport = event.port
        arp_table.update(inport, packet.protosrc, packet.hwsrc)
        route = routing_table.find_by_ip(packet.protosrc)
        if packet.protodst in route.split_ratios.keys():
            route.split_ratios[packet.protosrc] = route.split_ratios.pop(packet.protodst)

def launch():
    core.registerNew(ARPHandler)
