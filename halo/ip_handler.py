#
# Copyright(c) 2014 Waltz Networks Inc.
#

from pox.core import core
from pox.lib.util import dpid_to_str
from pox.lib.addresses import netmask_to_cidr
from pox.lib.packet.ipv4 import ipv4
from pox.lib.packet.udp import udp
from pox.lib.packet.tcp import tcp
from generic_handler import GenericHandler
from flow_table import FlowEntry
import pox.openflow.libopenflow_01 as of
from halo_utils import *

log = core.getLogger()


class IPHandler(GenericHandler):
    def __init__(self):
        super(IPHandler, self).__init__()
        core.addListeners(self)
        self.lost_buffers = {}
        self.outstanding_arps = {}

    def _handle_GoingUpEvent(self, event):
        core.openflow.addListeners(self)
        self.debug('IP up ...')

    def _handle_PacketIn(self, event):
        ev = self.parse_event(event)
        self.dpid = ev['dpid']

        if not ev['packet']:
            log.warning('%s: ignoring unparsed packet', dpid_to_str(ev['dpid']))
            return

        mac = ev['packet']
        # filter out non-ipv4 packets
        if not isinstance(mac.next, ipv4):
            return
        ip = mac.next

        if isinstance(ip.next, udp) or isinstance(ip.next, tcp):
            self.handle_ip(ev, mac)

    def handle_ip(self, ev, eth_frame):
        dpid = ev['dpid']
        inport = ev['inport']
        connection = ev['connection']
        ip = eth_frame.next
        arp_table = core.router[dpid].arp_table
        routing_table = core.router[dpid].routing_table
        interfaces = core.router[dpid].interfaces
        # send any waiting packets...
        self.send_lost_buffers(dpid, ip.srcip, eth_frame.src, inport)

        # learn mac from incoming port
        #arp_table.update(inport, ip.srcip, eth_frame.src)

        # check routing table, do we have a route for it?
        route = routing_table.find_by_ip(ip.dstip)
        if route is None:
            # send icmp unreachable
            self.debug('unable to find next hop for %s' % ip.dstip)
        else:
            # figure out which next hop to used in a route
            # remember which route this counters comes from
            flow = FlowEntry(inport = inport, srcip = ip.srcip, dstip = ip.dstip,
                             srcport = ip.next.srcport, dstport = ip.next.dstport)
             
            ip_str = IPAddr(route.subnet).toStr()
            key = '%s/%d' % (ip_str, route.netmask)
            next_hop = routing_table[key].select_next_hop(flow, inport, core.router[dpid])
            # figure out the mac address
            arp_entry = arp_table.lookup(next_hop)

            if arp_entry is None:
                for port in interfaces.iterkeys():
                    intf = interfaces.find_by_port(port[0])
                    if ipv4_apply_mask(intf.ip, netmask_to_cidr(intf.netmask)) == ipv4_apply_mask(ip.dstip, netmask_to_cidr(intf.netmask)):
                        arp = self.create_arp_request(intf.ip, ip.dstip, intf.hwaddr)
                        actions = of.ofp_action_output(port=port[0])
                        self.packet_out(ev, of.OFPP_NONE, arp, actions)
                        self.add_to_lost_buffers(ev, ip.dstip)
            else:
                # FIXME: install flow through multipath
                core.router[dpid].policy.set_flow_table(flow, arp_entry.hwaddr,
                                                           arp_entry.port)

                #print 'refresh split ratio'
                #routing_table[key].refresh_split_ratio()
                routing_table[key].compute_curr_split_ratios(core.router[dpid])

#                self.info("%i %i installing flow for %s => %s out port %i" %
#                        (dpid, inport, ip.srcip, ip.dstip, arp_entry.port))
#                actions = []
#                actions.append(of.ofp_action_dl_addr.set_dst(arp_entry.hwaddr))
#                actions.append(of.ofp_action_output(port = arp_entry.port))
#                matches = of.ofp_match.from_packet(eth_frame, inport)
#                matches.dl_src = None # Wildcard source MAC
#                msg = of.ofp_flow_mod(command=of.OFPFC_ADD,
#                                idle_timeout=10,
#                                hard_timeout=of.OFP_FLOW_PERMANENT,
#                                buffer_id=ev['ofp'].buffer_id,
#                                actions=actions,
#                                match=matches)
#                connection.send(msg.pack())

def launch():
    core.registerNew(IPHandler)
