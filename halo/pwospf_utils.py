#
# Copyright(c) 2014 Waltz Networks Inc.
#
import struct
from datetime import datetime
from halo_utils import ipv4_apply_mask
from pox.core import core
from pox.lib.addresses import IPAddr, netmask_to_cidr
from pox.lib.packet.ethernet import ETHER_BROADCAST
from pox.lib.recoco import Timer
from halo.pwospf import OSPF_PROTOCOL, OSPF_TYPE_HELLO, OSPF_TYPE_LSU
from halo.pwospf import OSPF_DEFAULT_HELLO_INT, OSPF_DEFAULT_LSU_INT
from halo.pwospf import pwospf, ls_hello, ls_upd, lsa
import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt

log = core.getLogger()

class NeighborRouter:
    def __init__(self, router_id=None, ip=None, last_rcvd_hello=None):
        self.rid = IPAddr(router_id)
        self.ip = ip
        self.last_rcvd_hello = last_rcvd_hello
        self._seq = 0
        self.ls_updates = dict()

    def __repr__(self):
        msg = "%s" % self.ip
        return msg

    @property
    def seq(self):
        return self._seq

    @seq.setter
    def seq(self, seq):
        self._seq = seq

    def refresh(self):
        self.last_rcvd_hello = datetime.now()

class Pwospfd():
    def __init__(self, dpid):
        self.dpid = dpid
        self._helloint = OSPF_DEFAULT_HELLO_INT
        self._lsuint = OSPF_DEFAULT_LSU_INT
        self._seq = struct.pack('h', -32768)

        self.hello_timer = Timer(self.helloint, self.send_hello, recurring = True)
        self.lsu_timer = Timer(self.lsuint, self.send_lsu, recurring = True)

    @property
    def helloint(self):
        return self._helloint

    @helloint.setter
    def helloint(self, v):
        if not isinstance(v, int):
            v = int(v)
        self._helloint = v

    @property
    def lsuint(self):
        return self._lsuint

    @lsuint.setter
    def lsuint(self, v):
        if not isinstance(v, int):
            v = int(v)
        self._lsuint = v

    @property
    def seq(self):
        return struct.unpack('h', self._seq)[0]

    @seq.setter
    def seq(self, seq):
        self._seq = struct.pack('h', seq)
        #self._seq = seq


    def change_helloint(self, new_int):
        self.hello_timer.cancel()
        self.hello_timer = Timer(new_int, self.send_hello, recurring = True)

    def change_lsuint(self, new_int):
        self.lsu_timer.cancel()
        self.lsu_timer = Timer(new_int, self.send_lsu, recurring = True)

    def packet_out(self, connection, eth_frame, actions):
        msg = of.ofp_packet_out()
        msg.data = eth_frame.pack()
        msg.actions.append(actions)
        connection.send(msg)

    def send_hello(self):
        router = core.router[self.dpid]
        connection = core.router[self.dpid].connection
        interfaces = core.router[self.dpid].interfaces
        for port in interfaces.iterkeys():
            if port != 65534: # ignore host port
                intf = interfaces.find_by_port(port[0])
                if intf.netmask is not None:
                    #log.info('port %d ip %s netmask %s' % (port[0], intf.ip, intf.netmask))
                    srcip = intf.ip
                    hello = self.create_pwospf_hello(intf.hwaddr, router.router_id, srcip, intf.netmask)
                    actions = of.ofp_action_output(port = port[0])
                    self.packet_out(connection, hello, actions)

    def send_lsu(self):
        router = core.router[self.dpid]
        connection = core.router[self.dpid].connection
        interfaces = core.router[self.dpid].interfaces
        routing_table = core.router[self.dpid].routing_table
        seqnum = -32768
        if self.seq < 32767:
            seqnum = self.seq + 1
        self.seq = seqnum
        for port in interfaces.iterkeys():
            if port[0] == 65534: # ignore host port
                continue

            intf = interfaces.find_by_port(port[0])
            if intf.netmask is None:
                continue

            #route = routing_table.find_by_ip(intf.ip)
            #if route is None:
                #continue

            srcip = intf.ip
            #dstip = route.next_hop
            lsu = self.create_pwospf_ls_update(intf.hwaddr, router.router_id, srcip, seqnum)
            actions = of.ofp_action_output(port = port[0])
            self.packet_out(connection, lsu, actions)

    # craft pwospf_lsu
    def create_pwospf_ls_update(self, hwsrc, rid, srcip, seq):
        # collect entries in routing table
        router = core.router[self.dpid]
        intfs = router.interfaces.get_all_intfs()

        lsu = ls_upd()
        lsu.seq = seq
        if lsu.seq < 32767 :
            lsu.ttl = 64
        else:
            lsu.ttl = 4
        lsu.n_adv = 0
        for i in range(len(intfs)):
            _lsa = lsa()
            _lsa.subnet = ipv4_apply_mask(IPAddr(intfs[i].ip), netmask_to_cidr(intfs[i].netmask))
            _lsa.netmask = intfs[i].netmask
            _lsa.tx_rate = router.policy.port_query.get_port_stats(intfs[i].port)['rate']
            if intfs[i].neighbors:
                _lsa.rid = intfs[i].neighbors[0].rid
            else:
                _lsa.rid = 0
            lsu.entries.append(_lsa)
            lsu.n_adv += 1

        e = pkt.ethernet(src=hwsrc, dst=ETHER_BROADCAST)
        e.type = pkt.ethernet.IP_TYPE
        ipp = pkt.ipv4(srcip=srcip, dstip=IPAddr('224.0.0.5'))
        ipp.protocol = OSPF_PROTOCOL
        ospfp = pwospf()
        ospfp.type = OSPF_TYPE_LSU
        ospfp.rid = rid
        ospfp.len = lsa.MIN_LEN * lsu.n_adv + ls_upd.MIN_LEN + pwospf.MIN_LEN

        ospfp.payload = lsu
        ipp.payload = ospfp
        e.payload = ipp
        return e


    # craft ethernet | ip | pwospf
    def create_pwospf_hello(self, hwsrc, rid, srcip, netmask):
        e = pkt.ethernet()
        e.src = hwsrc
        e.dst = ETHER_BROADCAST
        e.type = pkt.ethernet.IP_TYPE
        ipp = pkt.ipv4(srcip=srcip, dstip=IPAddr('224.0.0.5'))
        ipp.protocol = OSPF_PROTOCOL
        ospfp = pwospf()
        ospfp.type = OSPF_TYPE_HELLO
        ospfp.rid = rid
        ospfp.len = 32 #HELLO LEN

        hello = ls_hello()
        hello.netmask = netmask
        hello.helloint = self.helloint

        ospfp.payload = hello
        ipp.payload = ospfp
        e.payload = ipp
        return e

