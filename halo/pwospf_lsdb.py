#
# Copyright(c) 2014 Waltz Networks Inc.
#
# This file implements a basic topological database for pwospf. As a reference,
# you should read chapter 2 of OSPF specification:
# https://tools.ietf.org/html/rfc1583

from pox.core import core
from pox.lib.addresses import IPAddr, netmask_to_cidr
from pox.lib.util import dpid_to_str
from datetime import datetime
from adjacency_list import Topology
from halo_utils import ipv4_apply_mask
from pprint import pformat

pq = []
log = core.getLogger()

# Link State DB that stores raw LSA messages.
class LSDB(dict):
    # entires in lsdb
    class LSDB_Entry(object):
        lsa = dict() # use multi_key_dict
        def __init__(self, pwospf):
            self.rid = IPAddr(pwospf.rid)
            self.aid = IPAddr(pwospf.aid)
            self.last_update = datetime.now()
            self.seq = pwospf.next.seq
            self.csum = pwospf.csum
            self.lsa = dict()

        def check_seq(self, seq):
            passed = False
            if self.seq != seq and (seq > self.seq or seq == -32768):
                passed = True
            return passed

        # synchronize stored lsa with incoming lsa
        def populate_lsa(self, n, lsa):
            if n != len(lsa):
                return
            modified = False
            for i, val in enumerate(lsa):
                key = "%s/%s" %(IPAddr(val.subnet), IPAddr(val.netmask))
                if self.lsa.has_key(key):
                    tmp = self.lsa[key]
                    if tmp is not None and IPAddr(tmp.rid) != IPAddr(val.rid):
                        #log.info('exiting rid %s new rid %s' % (IPAddr(tmp.rid), IPAddr(val.rid)))
                        self.lsa[key] = val
                        modified = True
                    elif tmp is not None and tmp.tx_rate != val.tx_rate:
                        self.lsa[key] = val
                        modified = True
                else:
                    self.lsa[key] = val
                    modified = True
            return modified

    def __init__(self, dpid):
        super(LSDB, self).__init__()
        self.dpid = dpid

    def __repr__(self):
        o = []
        for idx, e in self.items():
            o.append('%s %17s %17s 0x%010x 0x%06x' % (IPAddr(e.rid),
                                                        IPAddr(e.aid),
                                                        e.last_update,
                                                        e.seq,
                                                        e.csum))
            for key, val in e.lsa.items():
                o.append('Entry:')
                o.append('\tSubnet: %s' % (IPAddr(val.subnet)))
                o.append('\tNetmask: %s' % (IPAddr(val.netmask)))
                o.append('\tRID: %s' % (IPAddr(val.rid)))
                o.append('\tTX_rate: %s' % (val.tx_rate))
        o.insert(0, "-- LinkState DB --")
        if len(o) == 1:
            o.append("<<Empty>>")
        return '\n'.join(o)

    # return True if modified existing entries
    def set(self, pwospf):
        rid = IPAddr(pwospf.rid)
        entry = self.get(rid)
        if entry is not None:
            r = self._update(entry, pwospf)
        else:
            r = self._insert(pwospf)
        return r

    # return router_id
    def get_router_id_by_ip(self, ip):
        for idx, e in self.items():
            for key, val in e.lsa.items():
                if val.subnet == ipv4_apply_mask(ip, netmask_to_cidr(val.netmask)):
                    return e.rid

    # new entry is created when received lsu from new neighbor.
    def _insert(self, pwospf):
        entry = self.LSDB_Entry(pwospf)
        lsu = pwospf.next
        entry.seq = lsu.seq
        entry.populate_lsa(lsu.n_adv, lsu.entries)
        self[entry.rid] = entry
        return True

    # existing entry is updated when received lsu from existing neighbor.
    def _update(self, entry, pwospf):
        modified = False
        lsu = pwospf.next
        #log.info('try to update lsdb existing entry')
        if entry.check_seq(lsu.seq):
            #log.info('updated lsdb existing entry %d' % lsu.seq)
            #new lsu is different from existing entry
            entry.seq = lsu.seq
            modified = entry.populate_lsa(lsu.n_adv, lsu.entries)
        return modified

    # remove entry from lsdb. This happens when link goes down, etc.
    def _delete(self, id):
        pass

    #FIXME: nithin should verify the correctness here!
    # return the latest topology
    def update_topology(self, router):
        topology = Topology()
        try:
            topology.addNode(router.router_id)
            for v in router.topology.nodeList[router.router_id].getConnections():
                if v != 0:
                    topology.addLink(router.router_id, v.getId(), router.policy.port_query.get_port_stats(router.interfaces.find_port_by_neighbor_router_id(v.getId()))['rate'])
        except:
            log.info('router not yet configured - update topology')
        # Add number of nodeis
        for idx, e in self.items():
            if e.rid != 0:
                topology.addNode(e.rid)
        # Add the links from LSDB
        log.debug(pformat(self.items()))
        for idx, e in self.items():
            if e.rid != 0:
                for key, val in e.lsa.items():
                    if val.rid !=0:
                        topology.addLink(e.rid, val.rid, val.tx_rate)
        # return updated topology
        return topology

    # get the shortest path to the given node
    def route(endNode):
        node = endNode
        labels = [endNode.label]
        # distances = []
        while node.label != node.prevNode.label:
            # distances.append(node.totalDistance - node.prevNode.totalDistance)
            node = node.prevNode
            labels.append(node.label)
        labels.reverse()
        return labels
        # distances.reverse()
        # return (labels, distances)

