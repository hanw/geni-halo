#
# Copyright(c) 2014 Waltz Networks Inc.
#
# Generic implementation of adjacency list
#

class Node:
    def __init__(self,key):
        self.id = key
        self.connectedTo = {}
        self.shortestPaths ={}
        self.eta = {}

    def addNeighbor(self,nbr,distance=0):
        self.connectedTo[nbr] = distance

    def addShortestPaths(self, dest, shortestpath):
        self.shortestPaths[dest] = shortestpath

    def addEta(self, dest, eta):
        self.eta[dest] = eta

    def hasNeighbor(self,nbr):
        return True if nbr in self.connectedTo else False

    def __str__(self):
        return str(self.id) + ' connectedTo: ' + str([x.id for x in self.connectedTo])

    def getConnections(self):
        return self.connectedTo.keys()

    def getId(self):
        return self.id

    def getDistance(self,nbr):
        return self.connectedTo[nbr]

class Topology:
    def __init__(self):
        self.nodeList = {}
        self.graph = {}
        self.numNodes = 0

    def __str__(self):
        pass

    def addNode(self,key):
        self.numNodes = self.numNodes + 1
        newNode = Node(key)
        self.nodeList[key] = newNode
        return newNode

    def getNode(self,n):
        if n in self.nodeList:
            return self.nodeList[n]
        else:
            return None

    def __contains__(self,n):
        return n in self.nodeList

    def addLink(self,f,t,cost=0):
        if f not in self.nodeList:
            self.addNode(f)
        if t not in self.nodeList:
            self.addNode(t)
        self.nodeList[f].addNeighbor(self.nodeList[t], cost)

    def getGraph(self):
        for v in self:
            self.graph[v.getId()] = {}
            for w in v.getConnections():
                self.graph[v.getId()][w.getId()] = self.nodeList[v.getId()].connectedTo[w]
        return self.graph

    def getNodes(self):
        return self.nodeList.keys()

    def __iter__(self):
        return iter(self.nodeList.values())

# Test
def main():
    g = Topology()
    for i in range(6):
        g.addNode(i)
    print g.nodeList
    g.addLink(0,1,5)
    g.addLink(0,5,2)
    g.addLink(1,2,4)
    g.addLink(2,3,9)
    g.addLink(3,4,7)
    g.addLink(3,5,3)
    g.addLink(4,0,1)
    g.addLink(5,4,8)
    g.addLink(5,2,1)

    for v in g:
        for w in v.getConnections():
            print('(%s, %s)' % (v.getId(), w.getId()))

if __name__ == '__main__':
    main()
