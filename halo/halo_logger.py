"""
Helper functions
"""
import logging

class HaloLogger(logging.LoggerAdapter):
  """
  Append DPID in front of logged message
  """
  def process(self, msg, kw):
    return '[%s] %s' % (self.extra['dpid'], msg), kw
