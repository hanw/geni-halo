#
# Copyright(c) 2014 Waltz Network Inc.
#
# This module manages cisco ios style configuration
#
import threading
from pox.core import core
from ciscoconfparse import CiscoConfParse
from netaddr import IPNetwork
from pox.lib.addresses import IPAddr, parse_cidr, netmask_to_cidr

from pyretic.backend.comm import *

log = core.getLogger()


class Confd():
    def __init__(self):
        core.Interactive.variables['config'] = self
        log.info('Confd up ...')

    # parse structured config file
    def parse(self, fname):
        parse = CiscoConfParse(fname)

        n = len(core.router.keys())
        if n == 1:
            router = core.router[core.router.keys()[0]]
        else:
            lines = parse.find_lines("^dpid")
            if len(lines) != 0:
                dpid = lines[0].split()[1]
                router = core.router[int(dpid)]
                log.info('router dpid %s' % dpid)
            else:
                return

        intfs = parse.find_lines("^interf")
        for intf in intfs:
            famobj = CiscoConfParse(parse.find_children(intf, exactmatch=True))
            print intf.split()
            _, _, port = intf.split()
            lines = famobj.find_lines("ip address")
            for line in lines:
                print line.split()
                _, _, ip, netmask = line.split()
            router.set_ip(port, ip, netmask)
            router.routing_table.add("%s/%d" % (IPAddr(ip), netmask_to_cidr(netmask)), {IPAddr(ip): 1}, router)

        routes = parse.find_lines("^ip route")
        #for route in routes:
            #_, _, ip, netmask, next_hop = route.split()
            #entry = "%s" % IPNetwork("%s/%s" % (ip, netmask))
            #print '%s %s' % (entry, next_hop)
            #router.add_route(entry, next_hop)

def launch():
    class async_loop(threading.Thread):
        def run(self):
            asyncore.loop()

    Confd()

    al = async_loop()
    al.daemon = True
    al.start()

