#
# Copyright 2014 Waltz Network Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Launch Pad
"""

def launch (settings=None):
    # debug message level
    from log.level import launch
    launch()

    # pretty-print debug message
    from samples.pretty_log import launch
    launch()

    # router instance, manages routing table and arp cache
    from halo.router import launch
    launch(settings)

    # handle arp packets, parse arp packets, generate update to arp cache.
    from halo.arp_handler import launch
    launch()

    # handle icmp packets
    from halo.icmp_handler import launch
    launch()

    # handle regular tcp/udp packets
    from halo.ip_handler import launch
    launch()

    # handle pwospf packets
    from halo.pwospf_handler import launch
    launch()

    # interactive console for debugging
    from halo.telnetd import launch
    launch()

    from halo.confd import launch
    launch()

