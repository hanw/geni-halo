#
# PWOSPF implementation in POX
#
from pox.core import core
from pox.lib.addresses import IPAddr, parse_cidr, netmask_to_cidr
from pox.lib.packet.ipv4 import ipv4
from pox.lib.packet.arp import arp
from pox.lib.packet.ethernet import ETHER_BROADCAST
from pox.lib.packet.packet_base import packet_base
from pox.lib.util import dpid_to_str
from pwospf import pwospf, ls_hello, ls_upd, lsa
from generic_handler import GenericHandler

from priodict import priorityDictionary
from adjacency_list import Topology
from routing_table import RoutingTable

import pox.lib.packet as pkt
import pox.openflow.libopenflow_01 as of

from halo_utils import ipv4_apply_mask
import halo.pwospf as pw

log = core.getLogger()

def Dijkstra(G,start,end=None):
	"""
	Find shortest paths from the start vertex to all
	vertices nearer than or equal to the end.

	The input graph G is assumed to have the following
	representation: A vertex can be any object that can
	be used as an index into a dictionary.  G is a
	dictionary, indexed by vertices.  For any vertex v,
	G[v] is itself a dictionary, indexed by the neighbors
	of v.  For any edge v->w, G[v][w] is the length of
	the edge.  This is related to the representation in
	<http://www.python.org/doc/essays/graphs.html>
	where Guido van Rossum suggests representing graphs
	as dictionaries mapping vertices to lists of neighbors,
	however dictionaries of edges have many advantages
	over lists: they can store extra information (here,
	the lengths), they support fast existence tests,
	and they allow easy modification of the graph by edge
	insertion and removal.  Such modifications are not
	needed here but are important in other graph algorithms.
	Since dictionaries obey iterator protocol, a graph
	represented as described here could be handed without
	modification to an algorithm using Guido's representation.

	Of course, G and G[v] need not be Python dict objects;
	they can be any other object that obeys dict protocol,
	for instance a wrapper in which vertices are URLs
	and a call to G[v] loads the web page and finds its links.
	
	The output is a pair (D,P) where D[v] is the distance
	from start to v and P[v] is the predecessor of v along
	the shortest path from s to v.
	
	Dijkstra's algorithm is only guaranteed to work correctly
	when all edge lengths are positive. This code does not
	verify this property for all edges (only the edges seen
 	before the end vertex is reached), but will correctly
	compute shortest paths even for some graphs with negative
	edges, and will raise an exception if it discovers that
	a negative edge has caused it to make a mistake.
	"""

	D = {}	# dictionary of final distances
	P = {}	# dictionary of predecessors
	Q = priorityDictionary()   # est.dist. of non-final vert.
	Q[start] = 0
	
	for v in Q:
		D[v] = Q[v]
		if v == end: break
		
		for w in G[v]:
			vwLength = D[v] + G[v][w]
			if w in D:
				if vwLength < D[w]:
					raise ValueError, \
  "Dijkstra: found better path to already-final vertex"
			elif w not in Q or vwLength < Q[w]:
				Q[w] = vwLength
				P[w] = v
	
	return (D,P)

def shortestPath(G,start,end):
    """
    Find a single shortest path from the given start vertex
    to the given end vertex.
    The input has the same conventions as Dijkstra().
    The output is a list of the vertices in order along
    the shortest path.
    """
    D,P = Dijkstra(G,start,end)
    Path = []
    while 1:
        Path.append(end)
        if end == start: break
        end = P[end]
    Path.reverse()
    return Path


class PwospfHandler (GenericHandler):
    def __init__ (self):
        super(PwospfHandler, self).__init__()
        core.addListeners(self)

    def _handle_GoingUpEvent(self, event):
        core.openflow.addListeners(self)
        self.debug('PWOSPF up...')

    def _handle_ConnectionUp (self, event):
        pass

    def _handle_PacketIn(self, event):
        ev = self.parse_event(event)
        self.dpid = ev['dpid']
        packet = ev['packet']

        #check message type, if not pwospf, return.
        ipp = packet.find('ipv4')
        if not ipp or not ipp.parsed:
            return

        if ipp.protocol != pw.OSPF_PROTOCOL:
            return

        p = pwospf(raw = ipp.payload, prev = self)

        if p.type == pw.OSPF_TYPE_HELLO:
            self.handle_ospf_hello(ev, ipp.srcip, ipp.dstip, p)
        elif p.type == pw.OSPF_TYPE_LSU:
            self.handle_ospf_lsu(ev, p)
        else:
            pass

    def validate_ospf_hello(self, ev, srcip, dstip, p):
        dpid = ev['dpid']
        port = ev['inport']
        interfaces = core.router[dpid].interfaces
        router = core.router[dpid]
        hello = p.next
        # drop hello packet if hello_int does not match
        if hello.helloint != router.get_ospf_helloint():
            self.error('invalid helloint')
            return False
        # drop hello packet if netmask does not match interface netmask
        #self.err("%s %s" % (hello.netmask, interfaces[port].netmask))
        if interfaces[port].netmask is None:
            self.err('invalid interface netmask %s' % interfaces[port].netmask)
            return False

        if IPAddr(hello.netmask) != IPAddr(interfaces[port].netmask):
            self.err('invalid netmask %s to %s' % (IPAddr(hello.netmask), interfaces[port].netmask))
            return False

        if ipv4_apply_mask(srcip, netmask_to_cidr(hello.netmask)) != ipv4_apply_mask(interfaces[port].ip, netmask_to_cidr(interfaces[port].netmask)):
            self.err('invalid neighbor ip')
            return False

        # dstip should be 224.0.0.5
        if dstip != IPAddr('224.0.0.5'):
            self.err('invalid dest ip')
            return False
        return True

    def handle_ospf_hello(self, ev, srcip, dstip, p):
        dpid = ev['dpid']
        router = core.router[dpid]
        routing_table = core.router[dpid].routing_table
        if self.validate_ospf_hello(ev, srcip, dstip, p) is not True:
            self.err('invalid ospf hello')
            return
        port = ev['inport']
        mac = ev['packet']
        arp_table = core.router[dpid].arp_table
        intf = core.router[dpid].interfaces.find_by_port(port)
        nbr = intf.find_neighbor_by_ip(srcip)
        if nbr is None:
            intf.add_neighbor(p.rid, srcip)
            route = routing_table.find_by_ip(srcip)
            for key, value in route.split_ratios.items():
                if ipv4_apply_mask(key, route.netmask) == route.subnet:
                    route.split_ratios[srcip] = route.split_ratios.pop(key)
            router.topology.addLink(router.router_id, p.rid, router.policy.port_query.get_port_stats(port)['rate'])
        else:
            nbr.refresh()

        # learn mac/port
        arp_table.update(port, srcip, mac.src)
        #self.print_hello(srcip, dstip)
        # inform other neighbors

    def handle_ospf_lsu(self, ev, p):
        dpid = ev['dpid']
        router = core.router[dpid]
        interfaces = core.router[dpid].interfaces
        routing_table = core.router[dpid].routing_table
        lsdb = core.router[dpid].lsdb
        try:
            # if from myself, drop
            self.debug('%s, %s' % (IPAddr(p.rid), IPAddr(core.router[dpid].router_id)))
            if IPAddr(p.rid) == IPAddr(core.router[dpid].router_id):
                self.err('received lsu from myself [%s], dropped' % IPAddr(p.rid))
                return
        except:
            log.info('switch not configured yet')

        """
        # add link from me to p
        me = IPAddr(router.router_id)
        nbr = IPAddr(p.rid)
        router.topology.addLink(me, nbr, 1)

        # add links from p to p's neighbors
        for idx, entry in enumerate(p.next.entries):
            if IPAddr(entry.rid) != IPAddr(0):
                nbr = IPAddr(p.rid)
                nbrnbr = IPAddr(entry.rid)
                router.topology.addLink(nbr, nbrnbr, 1)
        """
        # update lsdb
        run_dijkstra = lsdb.set(p)
        # update topology
        if run_dijkstra:

            self.forward_pwospf_ls_update(ev, p)
            router.topology = lsdb.update_topology(router)
            # execute dijkstra algorithm
            for w in router.topology:
                for v in router.topology:
                    if v != w:
                        try:
                            router.topology.nodeList[v.getId()].addShortestPaths(w.getId(), shortestPath(router.topology.getGraph(), v.getId(), w.getId()))
                        except KeyError:
                            self.err('key is not in path')
                        self.debug(router.topology.nodeList[v.getId()].shortestPaths)

                #Code to calculate \eta
                eta = {}
                for v in router.topology:
                    eta[v.getId()] = 1

                eta_parent = [w.getId()]

                while eta_parent:

                    eta_child = []
                    branch_counter = 0

                    for v in router.topology:

                        if v != w:
                            try:
                                if router.topology.nodeList[v.getId()].shortestPaths[w.getId()][1] == eta_parent[0]:
                                    branch_counter = branch_counter + 1
                                    eta_child = eta_child + [v.getId()]
                            except KeyError:
                                self.err('key is not in shortestPaths')

                    if eta_parent[0] != w:

                        for i in range(len(eta_child)):
                            eta[eta_child[i]] = eta[eta_parent[0]]*branch_counter

                    del eta_parent[0]
                    eta_parent = eta_parent + eta_child

                router.topology.nodeList[router.router_id].addEta(w.getId(), eta[w.getId()])

            self.update_route(router)
        else:
            self.debug('dijkstra is not triggered')

        #self.forward_pwospf_ls_update(ev, p)

    def update_route(self, router):

        a = 15000

        for v in router.topology.nodeList[router.router_id].shortestPaths:
            try:
                for key, val in router.lsdb[v].lsa.items():

                    # Retrieve current split ratios if any
                    current_split_ratios = router.routing_table.get_split_ratios(IPAddr(val.subnet), netmask_to_cidr(val.netmask))

                    # dictionary to store the new split ratios indexed by port
                    split_ratios = {}
                    alpha_change = 0
                    # get rate associated with this destination subnet and mask
                    rate = max (1, router.policy.flow_table.routing_table.get_rate(IPAddr(val.subnet), netmask_to_cidr(val.netmask)))
                    print rate
                    delta = a/(router.topology.nodeList[router.router_id].eta[v]*rate)


                    for i in router.interfaces.get_all_intfs():
                        for k in i.neighbors:
                            split_ratios[k.ip] = 0

                    next_hop_router_id = router.topology.nodeList[router.router_id].shortestPaths[v][1]
                    next_hop_router_ip = router.interfaces.find_neighbor_by_router_id(next_hop_router_id)
                    if current_split_ratios == None:

                        # If no split ratios then simply pick the shortest path
                        split_ratios[next_hop_router_ip] = 1
                    else:

                        ## Update the split ratios if split ratios exist
                        for k in split_ratios:
                            if k not in current_split_ratios.keys():
                                current_split_ratios[k] = 0
                            if k != next_hop_router_ip:
                                d_a = delta*current_split_ratios[k]
                                if delta <= 1:
                                    alpha_change = alpha_change + d_a
                                    split_ratios[k] = current_split_ratios[k] - d_a
                                else:
                                    alpha_change = alpha_change + current_split_ratios[k]
                                    split_ratios[k] = 0
                        split_ratios[next_hop_router_ip] = current_split_ratios[next_hop_router_ip] + alpha_change

                    # Update routes
                    network = "%s/%d" % (IPAddr(val.subnet), netmask_to_cidr(val.netmask))
                    router.routing_table.add(network, split_ratios, router)
                    # arp_table
                    #router.stats.set_stats(network, of.OFPP_CONTROLLER)
            except KeyError:
                self.err('key is not in lsdb')
                pass
        router.policy.update_policy()

    # compare old and new link state update, return True if the same.
    def is_route_the_same(self, lsu, route):

        n1, b1 = parse_cidr('%s/%s' % (IPAddr(lsu.subnet), IPAddr(lsu.netmask)))
        n2, b2 = parse_cidr('%s/%s' % (route.subnet, route.netmask))
        return True if (n1 == n2 and b1 == b2) else False

    def forward_pwospf_ls_update(self, ev, pwospf):
        interfaces = core.router[self.dpid].interfaces
        inport = ev['inport']
        packet = ev['packet']

        lsu = pwospf.next
        lsu.ttl = lsu.ttl - 1
        if lsu.ttl <= 0:
            return

        for port in interfaces.iterkeys():
            if port[0] == 65534:
                continue
            if port[0] == inport:
                continue

            intf = interfaces.find_by_port(port[0])
            ipp = packet.find('ipv4')
            ipp.srcip = intf.ip
            ipp.dstip = IPAddr('224.0.0.5')
            packet.src = intf.hwaddr
            packet.dst = ETHER_BROADCAST 
            pwospf.payload = lsu
            ipp.payload = pwospf
            packet.payload = ipp
            if ipp.srcip:
                #log.info('forward lsu: src %s inport %s outport %s' % (intf.ip, inport, port[0]))
                actions = of.ofp_action_output(port=port[0])
                self.packet_out(ev, of.OFPP_NONE, packet, actions)

def launch ():
    core.registerNew(PwospfHandler)


# Test
def main():
    pass    
'''    g = Topology()
    routingTable = RoutingTable()
    for v in g:
	for w in gi:
	    g.nodeList[v.getId()].addShortestPaths(w.getId(), shortestPath(g.getGraph(), v.getId(),w.getId())) 
    for v in g.nodeList[0].shortestPaths:
	if v != 0:
            routingTable.add(IPAddr(v).toStr()+'/32', g.nodeList[0].shortestPaths[v][1])    
    print routingTable
    routingTable.add(IPAddr(3).toStr()+'/32', 4)
    print routingTable
    print routingTable.route_id
    	
if __name__ == '__main__':
    main()
'''

