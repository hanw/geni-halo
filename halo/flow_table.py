#
# Copyright(c) 2014 Waltz Networks Inc.
#

from pox.lib.addresses import IPAddr
from pox.core import core

log = core.getLogger()

class FlowEntry(object):
    def __init__(self, inport=None, srcip=None, dstip=None, srcport=None, dstport=None):
        super(FlowEntry, self).__init__()
        self.inport = inport
        self.srcip = srcip
        self.dstip = dstip
        self.srcport = srcport
        self.dstport = dstport

    def __repr__(self):
        return "(%s)%s:%s->%s:%s" % (self.inport, self.srcip, self.srcport,
                                 self.dstip, self.dstport)

    def __eq__(self, other):
        return (isinstance(other, self.__class__)
                and self.__dict__ == other.__dict__)

class FlowTable(dict):
    def __init__(self, routing_table):
        super(FlowTable, self).__init__()
        self.routing_table = routing_table

    def __repr__(self):
        o = []
        for k, v in self.items():
            o.append((k,'%s, %s' %(k, v[3])))
        o.sort()
        o = [e[1] for e in o]
        o.insert(0, "-- Flow Table --")
        if len(o) == 1:
            o.append("<< Empty >>")
        return '\n'.join(o)

    # data in flow is a list of the following format
    # (port, dstmac, pkt_count, byte_count)
    def add_flow(self, flow, data):
        if len(data) != 4:
            log.err('invalid data len %d' % len(data))
            return
        self[flow] = data

    def del_flow(self, flow):
        if flow in self:
            del self[flow]

    # update flow table rate
    def update_flow_rate(self, key, v, interval):
        for f in self.keys():
            if f == key:
                old_v = self[f][2]
                if v >= old_v:
                    diff = v - old_v
                    self[f][2] = v
                    rate = float(diff / interval)
                    self[f][3] = rate
                return

    def update_routing_table(self):
        tmp_rate = {}
        for f in self.keys():
            route = self.routing_table.find_by_ip(f.dstip)
            ip_str = IPAddr(route.subnet).toStr()
            key = '%s/%d' % (ip_str, route.netmask)

            if key not in tmp_rate:
                tmp_rate[key] = 0
            tmp_rate[key] += self[f][3]
        for route in self.routing_table.keys():
            if route in tmp_rate:
                self.routing_table[route].rate = tmp_rate[route]
                print self.routing_table[route].rate
