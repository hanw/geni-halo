#
# Copyright(c) 2014 Waltz Network Inc.
#

from pyretic.core.runtime import *
from pyretic.backend.backend import *
from pyretic.lib.corelib import *
from pyretic.lib.std import *
from pox.core import core
from pprint import pformat
from halo_utils import ipv4_apply_mask, nw_addr_aton
from halo.flow_table import FlowEntry, FlowTable

log = core.getLogger()

# Collecting physical port statistics, one per router.
class PortQuery(PortBucket):
    def __init__(self):
        super(PortQuery, self).__init__()
        self.register_callback(self.callback)
        self.port_stats = {}
        import threading
        self.qthread = threading.Thread(target=self.qthread)
        self.qthread.daemon = True
        self.interval = 1
        self.qthread.start()

    def qthread(self):
        """
        Thread that send a query every fixed interval
        """
        while True:
            output = str(datetime.now()) + "| bucket " + str(id(self)) + ": print matches\n"
            for m in self.matches:
                output += str(m) + '\n'
            self.pull_stats()
            output += 'issued port query, going to sleep for %f' % self.interval
            #print output
            time.sleep(self.interval)

    def set_port_stats(self, port_no, previous_byte_count, current_byte_count, rate):
        self.port_stats[port_no] = {}
        self.port_stats[port_no]['previous_byte_count'] = previous_byte_count
        self.port_stats[port_no]['current_byte_count'] = current_byte_count
        self.port_stats[port_no]['rate'] = rate

    def get_port_stats(self, port_no):
        return self.port_stats[port_no]

    def callback(self, counts):
        if not self.port_stats:
            for k in counts[1].keys():
                self.port_stats[k] = {}
                self.port_stats[k]['previous_byte_count'] = counts[1][k]
                self.port_stats[k]['current_byte_count'] = counts[1][k]
                self.port_stats[k]['rate'] = 0
        else:
            for k in counts[1].keys():
                #print 'bytecount', k, counts[1][k]
                previous_byte_count = self.get_port_stats(k)['current_byte_count']
                rate = (counts[1][k] - previous_byte_count)/self.interval
                self.set_port_stats(k, previous_byte_count, counts[1][k], rate)

        #print '[port, packet cnt, byte cnt]', counts
        #print '[set_port_stats]', self.port_stats

# Collecting flow statistics, one per flow in flow table.
# TODO: optimize to aggregate related flows.
class FlowQuery(CountBucket):
    def __init__(self, flow_table, key):
        super(FlowQuery, self).__init__()
        self.register_callback(self.callback)
        import threading
        self.qthread = threading.Thread(target=self.qthread)
        self.qthread.daemon = True
        self.interval = 3
        self.flow_table = flow_table
        self.flowkey = key
        self.qthread.start()

    def qthread(self):
        """
        Thread that send a query every fixed interval
        """
        while True:
            output = str(datetime.now()) + "| bucket " + str(id(self)) + ": print matches\n"
            for m in self.matches:
                output += str(m) + '\n'
            self.pull_stats()
            output += 'issued flow query, going to sleep for %f' % self.interval
            #print output
            time.sleep(self.interval)

    # potential issue, where callback order is not maintained.
    def callback(self, counts):
        for k, v in counts[1].items():
            # k is a dict with 'srcip', 'dstip', 'srcport', 'dstport' as keys
            # v is bytecount for counts[1] or packetcount for counts[0]
            self.flow_table.update_flow_rate(k, v, self.interval)
        # trigger routing table rate recomputation
        self.flow_table.update_routing_table()

# Implement Multipath Policy, also add flow stats and port stats to policy
class PyreticPolicy(DynamicPolicy):
    """
    Policy that send packet on multipath, recalculated every time a
    new flow is added to the flow table.
    """
    def __init__(self, routing_table):
        super(PyreticPolicy, self).__init__()
        self.split_ratio = None
        self.flow_table = FlowTable(routing_table) # stores flow and its out-port
        self.query = {}
        self.port_query = PortQuery()
        self.update_policy()

    def set_flow_table(self, flow, dstmac, port):
        if not flow in self.flow_table.keys():
            # insert flow into flow table, with next hop port no.
            # [port, dstmac, bytecnt, rate]
            self.flow_table[flow] = [port, dstmac, 0, 0]
            self.query[flow] = FlowQuery(self.flow_table, flow)
            # change policy for next hop
            self.update_policy()

    # update_policy triggers the following activities in runtime.
    # - call DynamicPolicy.changed
    # - call DynamicPolicy.notify
    # - call runtime.handle_policy_change
    # - call runtime.update_dynamic_sub_pols
    # - compile rule into classifier
    # - install classifier in practive mode (proactive0 or proactive1)
    def update_policy(self):
        self.policy = parallel([(match(inport=k.inport) &
                                match(dstip=k.dstip.toStr()) &
                                match(srcip=k.srcip.toStr()) &
                                match(ethtype=0x0800) &
                                match(protocol=17) &
                                match(srcport=k.srcport) &
                                match(dstport=k.dstport)) >>
                                modify(dstmac=v[1].toStr()) >>
                                fwd(v[0]) + self.query[k]
                                for k, v
                                in self.flow_table.iteritems()] + [self.port_query])

        #self.policy += parallel([self.port_query])
        print ('current policy is: %s' % self.policy)

    def __repr__(self):
        try:
            return 'flow table: \n %s' % pformat(self.flow_table)
        except:
            return 'flow table'
