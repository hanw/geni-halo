#
# Copyright(c) 2014 Waltz Networks Inc.
#

import threading

import pox.openflow.libopenflow_01 as of
from pox.core import core
from pox.lib import revent, addresses as packetaddr, packet as packetlib

from pox.lib.util import dpid_to_str
from pyretic.backend.comm import *

log = core.getLogger()


class BackendChannel(asynchat.async_chat):
    """Sends messages to the server and receives responses.
    """
    def __init__(self, host, port, of_client):
        self.of_client = of_client
        self.received_data = []
        asynchat.async_chat.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.connect((host, port))
            print('connected to server %s %s' %(host, port))
        except e:
            exit(1)
        self.ac_in_buffer_size = 4096 * 3
        self.ac_out_buffer_size = 4096 * 3
        self.set_terminator(TERM_CHAR)
        return

    def handle_connect(self):
        log.info("Connected to pyretic frontend.")

    def collect_incoming_data(self, data):
        """Read an incoming message from the client and put it into our outgoing queue."""
        with self.of_client.channel_lock:
            self.received_data.append(data)

    def dict2OF(self,d):
        def convert(h,val):
            if h in ['srcmac','dstmac']:
                return packetaddr.EthAddr(val)
            elif h in ['srcip','dstip']:
                try:
                    return packetaddr.IPAddr(val)
                except:
                    return val
            elif h in ['vlan_id','vlan_pcp'] and val == 'None':
                return None
            else:
                return val
        return { h : convert(h,val) for (h, val) in d.items()}

    def found_terminator(self):
        """The end of a command or message has been seen."""
        with self.of_client.channel_lock:
            #print '***********'
            #print type(self.received_data)
            #print self.received_data
            msg = deserialize(self.received_data)

        # USE DESERIALIZED MSG
        if msg is None or len(msg) == 0:
            print "ERROR: empty message"
        elif msg[0] == 'packet':
            packet = self.dict2OF(msg[1])
            self.of_client.send_to_switch(packet)
        elif msg[0] == 'install':
            pred = self.dict2OF(msg[1])
            priority = int(msg[2])
            actions = map(self.dict2OF,msg[3])
            self.of_client.install_flow(pred,priority,actions)
        elif msg[0] == 'modify':
            pred = self.dict2OF(msg[1])
            priority = int(msg[2])
            actions = map(self.dict2OF,msg[3])
            self.of_client.modify_flow(pred,priority,actions)
        elif msg[0] == 'delete':
            pred = self.dict2OF(msg[1])
            priority = int(msg[2])
            self.of_client.delete_flow(pred,priority)
        elif msg[0] == 'clear':
            switch = int(msg[1])
            self.of_client.clear(switch)
        elif msg[0] == 'barrier':
            switch = msg[1]
            self.of_client.barrier(switch)
        elif msg[0] == 'flow_stats_request':
            switch = msg[1]
            self.of_client.flow_stats_request(switch)
        elif msg[0] == 'port_stats_request':
            switch = msg[1]
            self.of_client.port_stats_request(switch)
        else:
            print "ERROR: Unknown msg from frontend %s" % msg


class PyreticHandler(revent.EventMixin):

    # NOT **kwargs
    def __init__(self, show_traces=True, router=None, ip='127.0.0.1', port=BACKEND_PORT):
        self.router = router
        self.show_traces = show_traces
        self.packetno = 0
        self.channel_lock = threading.Lock()

        if core.hasComponent("openflow"):
            self.listenTo(core.openflow)

        self.backend_channel = BackendChannel(ip, port, self)

    def packet_from_network(self, **kwargs):
        return kwargs

    def packet_to_network(self, packet):
        return packet['raw']

    def send_to_pyretic(self,msg):
        serialized_msg = serialize(msg)
        try:
            with self.channel_lock:
                self.backend_channel.push(serialized_msg)
        except IndexError as e:
            print "ERROR PUSHING MESSAGE %s" % msg
            pass

    def send_to_switch(self,packet):
        switch = packet["switch"]
        outport = packet["outport"]
        try:
            inport = packet["inport"]
            if inport == -1 or inport == outport:
                inport = inport_value_hack(outport)
        except KeyError:
            inport = inport_value_hack(outport)

        msg = of.ofp_packet_out()
        msg.in_port = inport
        msg.data = self.packet_to_network(packet)
        msg.actions.append(of.ofp_action_output(port = outport))

        if self.show_traces:
            print "========= POX/OF SEND ================"
            print msg
            print packetlib.ethernet(msg._get_data())
            print

        ## HANDLE PACKETS SEND ON LINKS THAT HAVE TIMED OUT
        try:
            self.router.connection.send(msg)
        except RuntimeError, e:
            print "ERROR:send_to_switch: %s to switch %d" % (str(e),switch)
            # TODO - ATTEMPT TO RECONNECT SOCKET
        except KeyError, e:
            print "ERROR:send_to_switch: No connection to switch %d available" % switch
            # TODO - IF SOCKET RECONNECTION, THEN WAIT AND RETRY

    def build_of_match(self,switch,inport,pred):
        ### BUILD OF MATCH
        match = of.ofp_match()
        match.in_port = inport
        if 'srcmac' in pred:
            match.dl_src = pred['srcmac']
        if 'dstmac' in pred:
            match.dl_dst = pred['dstmac']
        if 'ethtype' in pred:
            match.dl_type = pred['ethtype']
        if 'vlan_id' in pred:
            match.dl_vlan = pred['vlan_id']
        if 'vlan_pcp' in pred:
            match.dl_vlan_pcp = pred['vlan_pcp']
        if 'protocol' in pred:
            match.nw_proto = pred['protocol']
        if 'srcip' in pred:
            match.set_nw_src(pred['srcip'])
        if 'dstip' in pred:
            match.set_nw_dst(pred['dstip'])
        if 'tos' in pred:
            match.nw_tos = pred['tos']
        if 'srcport' in pred:
            match.tp_src = pred['srcport']
        if 'dstport' in pred:
            match.tp_dst = pred['dstport']
        return match

    def build_of_actions(self,inport,action_list):
        ### BUILD OF ACTIONS
        of_actions = []
        for actions in action_list:
            outport = actions['outport']
            del actions['outport']
            if 'srcmac' in actions:
                of_actions.append(of.ofp_action_dl_addr.set_src(actions['srcmac']))
            if 'dstmac' in actions:
                of_actions.append(of.ofp_action_dl_addr.set_dst(actions['dstmac']))
            if 'srcip' in actions:
                of_actions.append(of.ofp_action_nw_addr.set_src(actions['srcip']))
            if 'dstip' in actions:
                of_actions.append(of.ofp_action_nw_addr.set_dst(actions['dstip']))
            if 'srcport' in actions:
                of_actions.append(of.ofp_action_tp_port.set_src(actions['srcport']))
            if 'dstport' in actions:
                of_actions.append(of.ofp_action_tp_port.set_dst(actions['dstport']))
            if 'vlan_id' in actions:
                if actions['vlan_id'] is None:
                    of_actions.append(of.ofp_action_strip_vlan())
                else:
                    of_actions.append(of.ofp_action_vlan_vid(vlan_vid=actions['vlan_id']))
            if 'vlan_pcp' in actions:
                if actions['vlan_pcp'] is None:
                    if not actions['vlan_id'] is None:
                        raise RuntimeError("vlan_id and vlan_pcp must be set together!")
                    pass
                else:
                    of_actions.append(of.ofp_action_vlan_pcp(vlan_pcp=actions['vlan_pcp']))
            if (not inport is None) and (outport == inport):
                of_actions.append(of.ofp_action_output(port=of.OFPP_IN_PORT))
            else:
                of_actions.append(of.ofp_action_output(port=outport))
        return of_actions

    def install_flow(self,pred,priority,action_list):
        switch = pred['switch']
        if 'inport' in pred:
            inport = pred['inport']
        else:
            inport = None
        match = self.build_of_match(switch,inport,pred)
        of_actions = self.build_of_actions(inport,action_list)
        msg = of.ofp_flow_mod(command=of.OFPFC_ADD,
                              priority=priority,
                              idle_timeout=of.OFP_FLOW_PERMANENT,
                              hard_timeout=of.OFP_FLOW_PERMANENT,
                              match=match,
                              actions=of_actions)
        try:
            self.router.connection.send(msg)
        except RuntimeError, e:
            print "WARNING:install_flow: %s to switch %d" % (str(e),switch)
        except KeyError, e:
            print "WARNING:install_flow: No connection to switch %d available" % switch

    def delete_flow(self,pred,priority):
        switch = pred['switch']
        if 'inport' in pred:
            inport = pred['inport']
        else:
            inport = None
        match = self.build_of_match(switch,inport,pred)
        msg = of.ofp_flow_mod(command=of.OFPFC_DELETE_STRICT,
                              priority=priority,
                              match=match)
        try:
            self.router.connection.send(msg)
        except RuntimeError, e:
            print "WARNING:delete_flow: %s to switch %d" % (str(e),switch)
        except KeyError, e:
            print "WARNING:delete_flow: No connection to switch %d available" % switch

    def modify_flow(self,pred,priority,action_list):
        switch = pred['switch']
        if 'inport' in pred:
            inport = pred['inport']
        else:
            inport = None
        match = self.build_of_match(switch,inport,pred)
        of_actions = self.build_of_actions(inport,action_list)
        msg = of.ofp_flow_mod(command=of.OFPFC_MODIFY_STRICT,
                              priority=priority,
                              idle_timeout=of.OFP_FLOW_PERMANENT,
                              hard_timeout=of.OFP_FLOW_PERMANENT,
                              match=match,
                              actions=of_actions)
        try:
            self.router.connection.send(msg)
        except RuntimeError, e:
            print "WARNING:modify_flow: %s to switch %d" % (str(e),switch)
        except KeyError, e:
            print "WARNING:modify_flow: No connection to switch %d available" % switch

    def barrier(self,switch):
        b = of.ofp_barrier_request()
        self.router.connection.send(b)

    def flow_stats_request(self,switch):
        sr = of.ofp_stats_request()
        sr.body = of.ofp_flow_stats_request()
        match = of.ofp_match()
        sr.body.match = match
        sr.body.table_id = 0xff
        sr.body.out_port = of.OFPP_NONE
        self.router.connection.send(sr)

    def port_stats_request(self, switch):
        sr = of.ofp_stats_request()
        sr.body = of.ofp_port_stats_request()
        sr.body.port_no = of.OFPP_NONE
        self.router.connection.send(sr)

    def clear(self,switch=None):
        if switch is None:
            for switch in self.switches.keys():
                self.clear(switch)
        else:
            d = of.ofp_flow_mod(command = of.OFPFC_DELETE)
            self.router.connection.send(d)

    def of_match_to_dict(self, m):
        h = {}
        if not m.in_port is None:
            h["inport"] = m.in_port
        if not m.dl_src is None:
            h["srcmac"] = m.dl_src.toRaw()
        if not m.dl_dst is None:
            h["dstmac"] = m.dl_dst.toRaw()
        if not m.dl_type is None:
            h["ethtype"] = m.dl_type
        if not m.dl_vlan is None:
            h["vlan_id"] = m.dl_vlan
        if not m.dl_vlan_pcp is None:
            h["vlan_pcp"] = m.dl_vlan_pcp
        if not m.nw_src is None:
            h["srcip"] = m.nw_src.toRaw()
        if not m.nw_dst is None:
            h["dstip"] = m.nw_dst.toRaw()
        if not m.nw_proto is None:
            h["protocol"] = m.nw_proto
        if not m.nw_tos is None:
            h["tos"] = m.nw_tos
        if not m.tp_src is None:
            h["srcport"] = m.tp_src
        if not m.tp_dst is None:
            h["dstport"] = m.tp_dst
        return h

    def of_actions_to_dicts(self, actions):
        action_dicts = []
        for a in actions:
            d = {}
            if a.type == of.OFPAT_OUTPUT:
                d['output'] = a.port
            elif a.type == of.OFPAT_ENQUEUE:
                d['enqueue'] = a.port
            elif a.type == of.OFPAT_STRIP_VLAN:
                d['strip_vlan_id'] = 0
            elif a.type == of.OFPAT_SET_VLAN_VID:
                d['vlan_id'] = a.vlan_vid
            elif a.type == of.OFPAT_SET_VLAN_PCP:
                d['vlan_pcp'] = a.vlan_pcp
            elif a.type == of.OFPAT_SET_DL_SRC:
                d['srcmac'] = a.dl_addr.toRaw()
            elif a.type == of.OFPAT_SET_DL_DST:
                d['dstmac'] = a.dl_addr.toRaw()
            elif a.type == of.OFPAT_SET_NW_SRC:
                d['srcip'] = a.nw_addr.toRaw()
            elif a.type == of.OFPAT_SET_NW_DST:
                d['dstip'] = a.nw_addr.toRaw()
            elif a.type == of.OFPAT_SET_NW_TOS:
                d['tos'] = a.nw_tos
            elif a.type == of.OFPAT_SET_TP_SRC:
                d['srcport'] = a.tp_port
            elif a.type == of.OFPAT_SET_TP_DST:
                d['dstport'] = a.tp_port
            action_dicts.append(d)
        return action_dicts

    def _handle_FlowStatsReceived (self, event):
        dpid = event.connection.dpid
        #print 'handle flow stats received %s' % dpid_to_str(dpid)
        def handle_ofp_flow_stat(flow_stat):
            flow_stat_dict = {}
            flow_stat_dict['table_id'] = flow_stat.table_id
            #flow_stat.match
            flow_stat_dict['duration_sec'] = flow_stat.duration_sec
            flow_stat_dict['duration_nsec'] = flow_stat.duration_nsec
            flow_stat_dict['priority'] = flow_stat.priority
            flow_stat_dict['idle_timeout'] = flow_stat.idle_timeout
            flow_stat_dict['hard_timeout'] = flow_stat.hard_timeout
            flow_stat_dict['cookie'] = flow_stat.cookie
            flow_stat_dict['packet_count'] = flow_stat.packet_count
            flow_stat_dict['byte_count'] = flow_stat.byte_count
            match = self.of_match_to_dict(flow_stat.match)
            flow_stat_dict['match'] = match
            actions = self.of_actions_to_dicts(flow_stat.actions)
            flow_stat_dict['actions'] = actions
            return flow_stat_dict
        flow_stats = [handle_ofp_flow_stat(s) for s in event.stats]
        self.send_to_pyretic(['flow_stats_reply',dpid,flow_stats])


    def _handle_PortStatsReceived(self, event):
        dpid = event.connection.dpid
        #print 'handle flow stats received %s' % dpid_to_str(dpid)
        def handle_ofp_port_stat(port_stat):
            port_stat_dict = {}
            port_stat_dict['port_no'] = port_stat.port_no
            port_stat_dict['rx_packets'] = port_stat.rx_packets
            port_stat_dict['tx_packets'] = port_stat.tx_packets
            port_stat_dict['rx_bytes'] = port_stat.rx_bytes
            port_stat_dict['tx_bytes'] = port_stat.tx_bytes
            port_stat_dict['rx_dropped'] = port_stat.rx_dropped
            port_stat_dict['tx_dropped'] = port_stat.tx_dropped
            port_stat_dict['rx_errors'] = port_stat.rx_errors
            port_stat_dict['tx_errors'] = port_stat.tx_errors
            port_stat_dict['rx_frame_err'] = port_stat.rx_frame_err
            port_stat_dict['rx_over_err'] = port_stat.rx_over_err
            port_stat_dict['rx_crc_err'] = port_stat.rx_crc_err
            port_stat_dict['collisions'] = port_stat.collisions
            return port_stat_dict
        port_stats = [handle_ofp_port_stat(s) for s in event.stats]
        self.send_to_pyretic(['port_stats_reply',dpid,port_stats])

#def launch():
#    class asyncore_loop(threading.Thread):
#        def run(self):
#            asyncore.loop()
#
#    PyreticHandler()
#
#    al = asyncore_loop()
#    al.daemon=True
#    al.start()

