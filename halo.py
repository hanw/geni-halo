#!/usr/bin/python
#
# Copyright(c) 2014 Waltz Network Inc.
#
from pyretic.core.runtime import Runtime
from pyretic.backend.backend import Backend
import sys
import signal
import subprocess
from importlib import import_module
import re
import os
import logging
from multiprocessing import Queue, Process
from logging import StreamHandler

halo_router = None

def signal_handler(signal, frame):
    print '\n----starting halo router shut down----'
    halo_router.kill()
    print 'halo.py down'
    sys.exit(0)

class QueueStreamHandler(StreamHandler):
    '''Relies on a multiprocessing.Lock to serialize multiprocess writes to a
    stream.'''

    def __init__(self, queue, stream=sys.stderr):
        self.queue = queue
        super(QueueStreamHandler, self).__init__(stream)

    def emit(self, record):
        '''Acquire the lock before emitting the record.'''
        self.queue.put(record)

def main():
    global halo_router

    # Set up multiprocess logging.
    verbosity_map = { 'low' : logging.WARNING,
                      'normal' : logging.INFO,
                      'high' : logging.DEBUG,
                      'please-make-it-stop' : logging.DEBUG }
    logging_queue = Queue()

    # Make a logging process.
    def log_writer(queue, log_level):
        formatter = logging.Formatter('%(levelname)s:%(name)s: %(message)s')
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        handler.setLevel(log_level)
        logger = logging.getLogger()
        logger.addHandler(handler)
        logger.setLevel(log_level)
        while(True):
            try:
                to_log = queue.get()
            except KeyboardInterrupt, e:
                print "\nkilling log"
                import sys
                sys.exit(0)
            logger.handle(to_log)
    log_level = verbosity_map.get('low', logging.DEBUG)
    log_process = Process(target=log_writer,args=(logging_queue, log_level,))
    log_process.daemon = True
    log_process.start()

    # Set default handler.
    logger = logging.getLogger()
    handler = QueueStreamHandler(logging_queue)
    logger.addHandler(handler)
    logger.setLevel(log_level)

    module = import_module('halo.stats.bucket')
    program = module.main
    kwargs = {}

    runtime = Runtime(Backend(),program,kwargs,'proactive0','low')
    try:
        output = subprocess.check_output('echo $PYTHONPATH',shell=True).strip()
    except:
        print 'Error: Unable to obtain PYTHONPATH'
        sys.exit(1)
    poxpath = None
    for p in output.split(':'):
         if re.match('.*pox/?$',p):
             poxpath = os.path.abspath(p)
             break
    if poxpath is None:
        print 'Error: pox not found in PYTHONPATH'
        sys.exit(1)

    pox_exec = os.path.join(poxpath, 'pox.py')
    python=sys.executable
    halo_router = subprocess.Popen([python,
                                    pox_exec,
                                    'halo.startup'],
                                    stdout = sys.stdout,
                                    stderr = subprocess.STDOUT)

    signal.signal(signal.SIGINT, signal_handler)
    signal.pause()

if __name__ == '__main__':
    main()
