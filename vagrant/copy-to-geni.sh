#!/usr/bin/env bash
set -x

if [ $# != 2 ]; then
    echo 'Usage: copy-to-geni.sh <LOCAL PATH> <REMOTE PATH>'
    exit 1
fi

SSH_OPTS="-q -o PreferredAuthentications=publickey -o HostbasedAuthentication=no -o PasswordAuthentication=no -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
KEY=~/.ssh/id_flakes_ssh_rsa
USER=hwang
machine=pc3.instageni.stanford.edu
PORT=(31293)

for i in "${PORT[@]}"; do
    scp -v -P $i $SSH_OPTS -i $KEY $1 $USER@${machine}:$2
done
