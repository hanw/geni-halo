#!/usr/bin/env/ bash
pushd $HOME #always execute at HOME
sudo dpkg-reconfigure bash
sudo apt-get update
sudo apt-get install -y python-dev python-pip
sudo pip install multi_key_dict ciscoconfparse netaddr bitarray networkx ipaddr

sudo cp packet-pwospf.so /usr/lib/wireshark/libwireshark1/plugins/

wget https://raw.github.com/frenetic-lang/pyretic/master/pyretic/backend/patch/asynchat.py
sudo mv asynchat.py /usr/lib/python2.7/
sudo chown root:root /usr/lib/python2.7/asynchat.py

wget https://bitbucket.org/hanw/asyncore/raw/462fc7bd2603f822500418d0aba617e7e892ba13/asyncore.py
sudo mv asyncore.py /usr/lib/python2.7/
sudo chown root:root /usr/lib/python2.7/asyncore.py

git clone https://github.com/noxrepo/pox.git
pushd pox
git checkout bab636b
popd

git clone https://github.com/frenetic-lang/pyretic.git
pushd pyretic
git checkout 28fa6ab
popd

git clone https://bitbucket.org/hanw/geni-halo.git
pushd geni-halo
ln -s ../pox pox
ln -s ../pyretic/pyretic pyretic
popd

sudo ovs-vsctl add-br br0
sudo ovs-vsctl add-port br0 eth1
sudo ovs-vsctl add-port br0 eth2
sudo ovs-vsctl add-port br0 eth3
sudo ovs-vsctl add-port br0 eth4
sudo ovs-vsctl set-controller br0 tcp:127.0.0.1:6633

sudo ovs-vsctl set-fail-mode br0 secure

pushd geni-halo/halo/conf_abilene
cp * /tmp/
popd


popd #back to where-ever

