#!/usr/bin/env bash
sudo update-alternatives --install /bin/sh sh /bin/bash 100

pushd $HOME #always execute from HOME
sudo apt-get update
sudo apt-get install -y tmux wireshark wireshark-dev scons ctags mosh

sudo cp packet-pwospf.so /usr/lib/wireshark/libwireshark1/plugins/

git clone https://github.com/CPqD/ofdissector.git
pushd ofdissector/src
export WIRESHARK=/usr/include/wireshark
scons install
sudo cp openflow.so /usr/lib/wireshark/libwireshark1/plugins/
popd

git clone https://github.com/hanw/dotvim.git
mv dotvim .vim
mv .vim/vimrc .vimrc
pushd .vim
git submodule init
git submodule update
popd
rm -rf dotvim

git clone https://github.com/hanw/dotmux.git
mv dotmux .mux
mv .mux/dot.tmux.conf .tmux.conf
echo 'export PATH=$PATH:~/.mux/' >> ~/.bashrc
source ~/.bashrc
rm -rf dotmux

popd # return to where-ever
