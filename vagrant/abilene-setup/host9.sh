#!/bin/sh 

set -x

route add -net 10.0.1.0 netmask 255.255.255.0 gw S9-lan8
route add -net 10.0.2.0 netmask 255.255.255.0 gw S9-lan8
route add -net 10.0.3.0 netmask 255.255.255.0 gw S9-lan8
route add -net 10.0.4.0 netmask 255.255.255.0 gw S9-lan8
route add -net 10.0.5.0 netmask 255.255.255.0 gw S9-lan8
route add -net 10.0.6.0 netmask 255.255.255.0 gw S9-lan8
route add -net 10.0.7.0 netmask 255.255.255.0 gw S9-lan8
route add -net 10.0.8.0 netmask 255.255.255.0 gw S9-lan8
route add -net 10.0.10.0 netmask 255.255.255.0 gw S9-lan8
route add -net 10.0.11.0 netmask 255.255.255.0 gw S9-lan8
