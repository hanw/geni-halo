#!/usr/bin/env/ bash
pushd $HOME #always execute at HOME

set -x

sudo ovs-vsctl add-br br0
sudo ovs-vsctl add-port br0 eth1
sudo ovs-vsctl add-port br0 eth2
sudo ovs-vsctl add-port br0 eth3
sudo ovs-vsctl add-port br0 eth4
sudo ovs-vsctl set-controller br0 tcp:127.0.0.1:6633

sudo pip install ipaddr bitarray networkx netaddr ciscoconfparse

#pushd geni-halo
#ln -s ../pox pox
#popd

pushd geni-halo/halo/conf_abilene
cp * /tmp/
popd

popd
