#!/usr/bin/env bash
set -x

if [ $# != 3 ]; then
    echo 'Usage: copy-from-geni.sh <PORT> <REMOTE PATH> <LOCAL PATH>'
    exit 1
fi

SSH_OPTS="-q -o PreferredAuthentications=publickey -o HostbasedAuthentication=no -o PasswordAuthentication=no -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
KEY=~/.ssh/id_flakes_ssh_rsa
USER=hwang
machine=pc3.instageni.stanford.edu

scp -v -P $1 $SSH_OPTS -i $KEY $USER@${machine}:$2 $3
