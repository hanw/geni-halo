#!/usr/bin/env bash
sudo ovs-vsctl add-br br0
sudo ovs-vsctl add-port br0 eth1
sudo ovs-vsctl add-port br0 eth2
sudo ovs-vsctl add-port br0 eth3

sudo ovs-vsctl set-controller br0 tcp:127.0.0.1:6633
