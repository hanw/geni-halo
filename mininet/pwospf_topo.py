#!/usr/bin/python

"""
Start up the topology for PWOSPF
"""

from mininet.net import Mininet
from mininet.node import OVSSwitch, RemoteController
from mininet.log import setLogLevel, info
from mininet.cli import CLI
from mininet.topo import Topo

from sys import exit
import os.path

IPCONFIG_FILE = 'IP_CONFIG'
IP_SETTING={}

class Loop (Topo):
  "Topology that contains a loop"

  def __init__ (self):
    "Create custom topology"

    #init
    Topo.__init__(self)

    #Add host and switches
    h1 = self.addHost('h1')
    h2 = self.addHost('h2')
    h3 = self.addHost('h3')

    s1 = self.addSwitch('s1')
    s2 = self.addSwitch('s2')
    s3 = self.addSwitch('s3')

    #Add links
    self.addLink( h1, s1 )
    self.addLink( h2, s2 )
    self.addLink( h3, s3 )
    self.addLink( s1, s2 )
    self.addLink( s1, s3 )
    #self.addLink( s2, s3 )

c1 = RemoteController('c1', ip='192.168.56.1', port=6633)
c2 = RemoteController('c2', ip='192.168.56.1', port=6633)
c3 = RemoteController('c3', ip='192.168.56.1', port=6633)

cmap = {'s1': c1, 's2': c2, 's3': c3}

class MultiSwitch( OVSSwitch ):
  "Custom switch class that uses multiple controllers"
  def start (self, controllers):
    return OVSSwitch.start(self, [cmap[self.name]])

def set_default_route(host):
    info('*** setting default gateway of host %s\n' % host.name)
    if(host.name == 'h1'):
        routerip = IP_SETTING['s1-eth1']
    elif(host.name == 'h2'):
        routerip = IP_SETTING['s2-eth1']
    elif(host.name == 'h3'):
        routerip = IP_SETTING['s3-eth1']

    host.cmd('route add default gw %s dev %s-eth0' % (routerip, host.name))

def get_ip_setting():
    if (not os.path.isfile(IPCONFIG_FILE)):
        return -1
    f = open(IPCONFIG_FILE, 'r')
    for line in f:
        if( len(line.split()) == 0):
          break
        name, ip = line.split()
        print name, ip
        IP_SETTING[name] = ip
    return 0

def simpleNet():
    r = get_ip_setting()
    if r == -1:
        exit("Couldn't load config file for ip addresses, check whether %s exists" % IPCONFIG_FILE)
    else:
        info( '*** Successfully loaded ip settings for hosts\n %s\n' % IP_SETTING)

    topo = Loop()
    info( '*** Creating network\n' )
    net = Mininet(topo=topo, switch=MultiSwitch, build=False)
    for c in c1, c2, c3:
      net.addController(c)

    net.build()
    net.start()

    h1, h2, h3= net.get( 'h1', 'h2', 'h3')
    for h in h1, h2, h3:
      intf=h.defaultIntf()
      intf.setIP('%s/24' % IP_SETTING[h.name])
      set_default_route(h)

    CLI( net )

    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    simpleNet()
