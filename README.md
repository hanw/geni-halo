#
# README
#

Run:

1. create symlink to pox, at project root directory, execute:
   > ln -s <PATH to POX> pox
2. add application to python search path:
   > cd pox_module
   > sudo python setup.py develop
3. create mininet topology
   sh run_mininet.sh
4. run halo controller
   sh run_pox.sh

DEBUG Environment Setup:

We test halo-geni against pwospf reference design by Huangty. First, you need to
checkout git repo at https://bitbucket.org/huangty/pwospf into /home directory.

1. setup topology:
   sh run_mininet.sh
2. setup halo controller, go back to halo project directory
   sh run_pox.sh
3. start reference software router instance.
   sh run_sr.sh <IP of interface that runs mininet> vhost<1,2,3>

In the mininet shell, you can do 'xterm client' or 'xterm server<1,2>' to login to
nodes created by mininet. Use 'wireshark' to inspect packet in/out these nodes.


